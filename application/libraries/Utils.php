<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

define('_HASHSALT_', 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');
define('_HASHKEY_', '99iloveORANGEdesk99'); 

class Utils {



	const success = 1;
	const empty = 2;
	const unmatch = 3;
	const invalid = 4;

	const active = 1;
	const inactive = 2;

	const exists = 1;
	const nonexists = 2;

	const superadmin = 1;
	const admin = 2;
	const receptionist = 3;

	const solo = 1;
	const hall = 2;
	const group = 3;
	const weekly = 4;
	const monthly = 5;
	const free = 6;

	const transaction_pending = 1;
	const transaction_validate = 2;
	const transaction_extend = 3;
	const transaction_overdue = 4;
	const transaction_done = 5;

	const max_hours = 24;
	const default_password = 'iloveorange2019';
	const default_password_receptionist = 'orangedesk2019';


	const day = 24;
	const promo = 4;
	const vip = 5;

	const one= 1;
	const two = 2;

	const professional = 1;
	const student = 2;

	// const amount_p_day = 1000;
	// const amount_p_promo = 800;
	// const amount_p_one = 600;
	// const amount_p_two = 400;
	
	// const amount_s_day = 100;
	// const amount_s_promo = 80;
	// const amount_s_one = 60;
	// const amount_s_two = 40;

	const firstgracetime = 11;
	const secondgracetime = 31;



	public function generateRandomPassword($length = 8) {

    	return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}

	public function getNowForMysql() {
		$this->defaultTimeZone();
		return date('Y-m-d H:i:s');
	}

	public function formatDateTimeForMysql(\DateTime $d) {
		if ($d) {
			return $d->format('Y-m-d h:i:s');
		}
		return null;
	}

	public function getNowStartDate() {
		$this->defaultTimeZone();

		$presenthour =  $this->getCurrentHour();

		if(in_array($presenthour, array(0,1,2,3,4,5))){
			$from = date('Y-m-d', strtotime($this->getCurrentYear().'-'.$this->getCurrentMonth().'-'.$this->getCurrentDay(). ' - 1 days'));
			
			return $from.' 06:00:00';

			
		}else{
			return date('Y-m-d ').'06:00:00';
		}
		
		// 
	}

	public function getCurrentHour() {
		$this->defaultTimeZone();
		return date('H');
	}

	public function getNowEndDate() {
		$this->defaultTimeZone();

		// $to = date('Y-m-d', strtotime($this->getCurrentYear().'-'.$this->getCurrentMonth().'-'.$this->getCurrentDay(). ' + 1 days'));
		
		// return $to.' 05:59:59';

		$presenthour =  $this->getCurrentHour();

		if(in_array($presenthour, array(0,1,2,3,4,5))){
			
			return date('Y-m-d ').'05:59:59';
			
		}else{
			

			$to = date('Y-m-d', strtotime($this->getCurrentYear().'-'.$this->getCurrentMonth().'-'.$this->getCurrentDay(). ' + 1 days'));
			
			return $to.' 05:59:59';
		}


	}

	public function getRecordType($type) {

		switch ($type) {
		    case self::solo:
		        $record_type = 'Solo';
		        break;
		    case self::hall:
		        $record_type = 'Orange Room';
		        break;
		    case self::group:
		        $record_type = 'Seminar';
		        break;
		    case self::weekly:
		        $record_type = 'Weekly Pass';
		        break;
		    case self::monthly:
		        $record_type = 'Monthly Pass';
		        break; 
		    default:
		    	$record_type = 'Free Pass';
		}

		return $record_type;
	}

	public function getTrasactionStatus($status,$datetime,$hours,$datesettled,$recordtype = null, $date_to = null) {
		// print_r($date_to);exit(); 
		//||  (in_array($recordtype, array(utils::free)) && $datetime >= $this->getNowForMysql())
		$overdue  = $this->isOverdue($status,$datetime,$hours,$datesettled);
		switch (true) {
		    case ($status == self::transaction_validate && $overdue == false && in_array($recordtype, array(utils::solo,utils::group,utils::hall)))  || (in_array($recordtype, array(utils::weekly,utils::monthly,utils::free)) && $date_to >= $this->getNowForMysql() and $status != self::transaction_done):
		        $transaction_type = '<button type="button" class="btn btn-info btn-xs">Verified</button>';
		        break;
		    case ( in_array($recordtype, array(utils::weekly,utils::monthly,utils::free)) && $date_to <= $this->getNowForMysql() and $status != self::transaction_done ):
		        $transaction_type = '<button type="button" class="btn btn-warning btn-xs">Expired</button>';
		        break;
		    case $status == self::transaction_extend && $overdue == false:
		    	$transaction_type = '<button type="button" class="btn btn-warning btn-xs">Extended</button>';
		        break;
		    case $overdue ==true && in_array($recordtype,array(utils::solo,utils::group,utils::hall)):
		        $transaction_type = '<button type="button" class="btn btn-danger btn-xs">Overdue</button>';
		        break;
		    case $status == self::transaction_done:
		        $transaction_type = '<button type="button" class="btn btn-success btn-xs">Done</button>';
		        break;
		    default:
		}

		return $transaction_type;
	}

	public function getCurrentMonth() {
		$this->defaultTimeZone();
		return date('m');
	}

	public function getCurrentYear() {
		$this->defaultTimeZone();
		return date('Y');
	}

	public function getCurrentDay() {
		$this->defaultTimeZone();
		return date('d');
	}

	public function getFullNameMonth() {
		$this->defaultTimeZone();
		return date('M');
	}

	public function getDateTimeFormat($date){
		$dateTime = new DateTime($date);
		return $dateTime->format('M j, Y, g:i a');
	}

	public function getNewDateTime($datetime,$hours)
	{
		$this->defaultTimeZone();
		if(strpos($hours, '.') === false){
			return  date('Y-m-d H:i:s',strtotime('+'.$hours.' hour',strtotime($datetime)));
		}else{
			$mins = $hours * 60;
			return date('Y-m-d H:i:s',strtotime('+'.$mins.' minutes',strtotime($datetime)));
		}

	}


	public function getAdjustedDateTime(){
	


		$presenthour = $this->getCurrentHour();

		if(!in_array($presenthour,array(0,1,2,3,4,5))){

			$from = $this->getCurrentMonth().'/'.$this->getCurrentDay().'/'.$this->getCurrentYear().' 06:00 AM';
			$to = date('Y-m-d', strtotime($this->getCurrentYear().'-'.$this->getCurrentMonth().'-'.$this->getCurrentDay(). ' + 1 days'));
			$date = explode('-', $to);
			$to = $date[1].'/'.$date[2].'/'.$date[0].' 05:59 AM';

		}else{

			$from = date('Y-m-d', strtotime($this->getCurrentYear().'-'.$this->getCurrentMonth().'-'.$this->getCurrentDay(). ' - 1 days'));
			$frm = explode('-', $from);
			$from = $frm[1].'/'.$frm[2].'/'.$frm[0].' 06:00 AM';

			$to = date('Y-m-d');
			$t = explode('-', $to);
			$to = $t[1].'/'.$t[2].'/'.$t[0].' 05:59 AM';

		}


		return $from .' - '. $to;
	}

	public function defaultTimeZone() {
		return date_default_timezone_set('Asia/Manila');
	}

	public function getTimeOverdue($datetime,$hours,$settled_date) {
		$datetime1 = new DateTime($this->getNewDateTime($datetime,$hours));
		

		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}

		$result = 0;
		if($datetime2 >= $datetime1){
			$interval = $datetime1->diff($datetime2);
			$result = $interval->format('%d')." Days ".$interval->format('%h')." Hrs ".$interval->format('%i')." Mins";
		}
		// print_r($result);exit();
		return  $result;
	}
	
	public function getTimeRemaining($datetime,$hours,$settled_date) {

		if($hours == utils::vip){
			$hours  = $hours - 1;
		}

		// print_r(array($hours));exit();

		$datetime1 = new DateTime($this->getNewDateTime($datetime,$hours));
		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}
		

		$result = 0;
		if($datetime2 <= $datetime1){
			$interval = $datetime2->diff($datetime1);
			$result = $interval->format('%d')." Days ".$interval->format('%h')." Hrs ".$interval->format('%i')." Mins";
		}
		return  $result;
	}

	public function getRemaining($verify_date,$hours,$settled_date){


		// if($hours == utils::vip){
		// 	$hours  = $hours - 1;
		// }

		// print_r(array($hours));exit();

		$datetime1 = new DateTime($this->getNewDateTime($verify_date,$hours));


		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}
		

		$result = 0;
		if($datetime2 <= $datetime1){
			$interval = $datetime2->diff($datetime1);
			$result =  ($interval->h * 60 ) + ($interval->i) + ($interval->days*24*60);
		}

		return  $result;
	}


	public function getOverdued($verify_date,$hours,$settled_date){


		if($hours == utils::vip){
			$hours  = $hours - 1;
		}

		// print_r(array($hours));exit();

		$datetime1 = new DateTime($this->getNewDateTime($verify_date,$hours));


		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}
		

		$result = 0;
		if($datetime2 >= $datetime1){
			$interval = $datetime2->diff($datetime1);
			$result =  ($interval->h * 60 ) + ($interval->i) + ($interval->days*24*60);
		}

		return  $result;
	}

	public function totalTimeCovered($verify_date,$hours,$settled_date){

		if($hours == utils::vip){
			$hours  = $hours - 1;
		}

		// print_r(array($hours));exit();


		$datetime1 = new DateTime($this->getNewDateTime($verify_date,$hours));

		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}

		if($datetime2 >= $datetime1){
			$interval = $datetime2->diff($datetime1);
			$result =  ($interval->h * 60 ) + ($interval->i) + ($interval->days*24*60) + ($hours * 60);
		}else{
			$interval = $datetime2->diff($datetime1);
			$result = ($hours * 60) - (($interval->h * 60 ) + ($interval->i) + ($interval->days*24*60));
		}

		return $result;
	}

	public function getTodayFormat() 
	{
		return $this->getCurrentYear().$this->getCurrentMonth().$this->getCurrentDay();
	}


	public function convertToHoursMins($time,$recordtype=null) {



	    if ($time < 1 || in_array($recordtype, array(utils::weekly,utils::monthly,utils::free))) {
	        return '' ;
	    }
	    $days = ($time/24)/60;
	    $hours = ($time / 60)%24;
	    $minutes = ($time % 60);
	    return sprintf('%2d Days, %02d Hrs, %02d Mins',$days, $hours, $minutes);
	}

	public function isOverdue($status,$datetime,$hours,$settled_date,$record_type = null) {

		$datetime1 = new DateTime($this->getNewDateTime($datetime,$hours));
		if(!empty($settled_date)){
			$datetime2 = new DateTime($settled_date);
		}else{
			$datetime2 = new DateTime($this->getNowForMysql());
		}

		$result = false;
		if(!empty($status)){
			if($datetime2 > $datetime1 && $status != self::transaction_done  && !in_array($record_type, array(utils::weekly,utils::monthly,utils::free)) ){
				$result = true;
			}
		}else{
			if($datetime2 > $datetime1 && in_array($record_type, array(utils::solo,utils::group,utils::hall)) ){
				$result = true;
			}
			
		}

		return $result;
	}

	public function getTotalEarnToday($amount, $verify_date = null, $hours = null, $settled_date =null, $ratesperhour_solo = null, $record_type = null,$ratesperhour_two = null ,$ratesperhour_promo = null,$rates = null,$ratesperhour_days = null){

		$vip = $hours;

		$amountregistered = 0; $succeeding = 30;
		switch (true) {
			case in_array($record_type, array(utils::weekly, utils::monthly, utils::free)):
					return $amount;
				break;
			case $record_type == utils::solo:
					$overdue = $this->getOverdued($verify_date,$hours,$settled_date);
					// 
					if($vip == utils::vip){
						$hours =  $hours - 1;
					}

					if($overdue > 0){
						$time = ($hours * 60) + $this->getOverdued($verify_date,$hours,$settled_date);

					}else{
						$time = $hours * 60;
					}
				break;
			case in_array($record_type, array(utils::group, utils::hall)):
					$time = ($hours * 60) + $this->getOverdued($verify_date,$hours,$settled_date);
				break;
			default:
				$time = 0;
				break;
		}



	 	$hours = floor($time / 60); //floor($time / 60)
	    $minutes =($time % 60); //

			   

	    if( $record_type == utils::solo){ //$hours < utils::promo and
	    	


	    	if( $hours == utils::one ){
	    		$exceed = $ratesperhour_solo;

	    		// print_r(array($hours,$minutes));exit();

	    		if( ($minutes >= self::firstgracetime and $minutes <  self::secondgracetime) ){
		    		$exceed = $exceed + (0.5*$succeeding);
			    }else if($minutes < self::firstgracetime){
			    	$exceed = $exceed;
			    }else{
			    	$exceed = $ratesperhour_two;
			    }

	    	}elseif( $hours == utils::two   ){
	    		$exceed = $ratesperhour_two;

	    		if( ($minutes >= self::firstgracetime and $minutes <  self::secondgracetime) ){
		    		$exceed = $exceed + (0.5*$succeeding);
			    }else if($minutes < self::firstgracetime){
			    	$exceed = $exceed;
			    }else{
			    	$exceed = $exceed + 20;
			    }

	    	}elseif((in_array($hours, array(3,4)) ) or ($hours == utils::two and $minutes > self::secondgracetime)){
	    		$exceed = $ratesperhour_promo;
                // and $minutes < self::firstgracetime
	    		// print_r($exceed);exit();
	    		
	    		if($hours == utils::promo){
	    			if( ($minutes >= self::firstgracetime and $minutes <  self::secondgracetime) ){
		    			$exceed = $exceed + (0.5*$succeeding);
				    }else if($minutes < self::firstgracetime){
				    	$exceed = $exceed;
				    }else{
				    	$exceed = $exceed + (1*$succeeding);
				    }
	    		}

	    	}elseif( ($hours >= utils::promo or (in_array($hours, array(3,4)) and $minutes > self::secondgracetime)) and $hours < utils::day ){
	    		$exceed =0;
	    		// print_r(array($hours)); 

	    		if($hours >= 4 or $hours == 3 ){
	    			$exceed  = $ratesperhour_promo;
	    		}

	    		$exceed = $exceed + (($hours - utils::promo) * $succeeding) ;



		    	if( ($minutes >= self::firstgracetime and $minutes <  self::secondgracetime) ){
		    		$exceed = $exceed + (0.5*$succeeding);
			    }else if($minutes < self::firstgracetime){
			    	$exceed = $exceed;
			    }else{
			    	$exceed = $exceed + (1*$succeeding);
			    }
			 
			 //   if( ($minutes > self::firstgracetime and $minutes <  self::secondgracetime) ){
		  //  		$exceed = $exceed + (0.5*$succeeding);
			 //   }else {
			 //   	$exceed = $exceed;
			 //   }
			 

			    // print_r(array($exceed));exit();
			
			    // $exceed  = $exceed + $amount;
			    


			   


	    	}elseif ($hours == utils::day) {
	    		$exceed = $ratesperhour_days;

	    	}else{
	    		$exceed = $ratesperhour_days;
	    	}
	    	
	    	 if($exceed >= $ratesperhour_days){
			    $exceed = $ratesperhour_days;
			 }

			 


	    }else{

	    	

	    	$exceed = $hours * $rates;
	    	if( ($minutes >= self::firstgracetime and $minutes <  self::secondgracetime) ){
	    		$exceed = $exceed + (0.5*$rates);
		    }else if($minutes < self::firstgracetime){
		    	$exceed = $exceed;
		    }else{
		    	$exceed = $exceed + (1*$rates);
		    }
	    }

	    // print_r(array($hours,$minutes,$exceed,$minutes));
	   
	    $total = $amountregistered + $exceed;

	    // print_r(array($total));exit();

		if($total>0){
			// return number_format($total,2);

			return $total;
		}
		return number_format(0,2);
	}


	public function _base64_encrypt($str,$passw=_HASHKEY_){

		$r='';
		$md=$passw?substr(md5($passw),0,16):'';
		$str=base64_encode($md.$str);
		$a=str_split('+/='._HASHSALT_);
		$b=strrev('-_='._HASHSALT_);
		if($passw){
			$b=$this->_mixing_passw($b,$passw);
		}else{
			$r=rand(10,65);
			$b=mb_substr($b,$r).mb_substr($b,0,$r);
		}
		$s='';
		$b=str_split($b);
		$str=str_split($str);
		$lens=count($str);
		$lena=count($a);
		for($i=0;$i<$lens;$i++){
			for($j=0;$j<$lena;$j++){
				if($str[$i]==$a[$j]){
					$s.=$b[$j];
				}
			};
		};
		return $s.$r;
	}

	public function _base64_decrypt($str,$passw=_HASHKEY_){
		$a=str_split('+/='._HASHSALT_);
		$b=strrev('-_='._HASHSALT_);
		if($passw){
			$b=$this->_mixing_passw($b,$passw);
		}else{
			$r=mb_substr($str,-2);
			$str=mb_substr($str,0,-2);
			$b=mb_substr($b,$r).mb_substr($b,0,$r);
		}
		$s='';
		$b=str_split($b);
		$str=str_split($str);
		$lens=count($str);
		$lenb=count($b);
		for($i=0;$i<$lens;$i++){
			for($j=0;$j<$lenb;$j++){
				if($str[$i]==$b[$j]){
					$s.=$a[$j];
				}
			};
		};
		$s=base64_decode($s);
		if($passw&&substr($s,0,16)==substr(md5($passw),0,16)){
			return substr($s,16);
		}else{
			return $s;
		}
	}

	public function _mixing_passw($b,$passw){
		$s='';
		$c=$b;
		$b=str_split($b);
		$passw=str_split(sha1($passw));
		$lenp=count($passw);
		$lenb=count($b);
		for($i=0;$i<$lenp;$i++){
			for($j=0;$j<$lenb;$j++){
				if($passw[$i]==$b[$j]){
					$c=str_replace($b[$j],'',$c);
					if(!preg_match('/'.$b[$j].'/',$s)){
						$s.=$b[$j];
					}
				}
			};
		};
		return $c.''.$s;
	}



	
	
}
