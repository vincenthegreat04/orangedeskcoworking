<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				
	<?php
		$this->load->view('nav');
	?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12" >
								<div class="x_panel">
									<div class="x_title">
										<h2><small><span class="fa fa-money"></span> Rate List</small></h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
											<li class="dropdown">
												<a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#addrate"><i class="fa fa-plus"></i></a>
											</li> 
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										
										<table id="ratelist" class="table table-striped" style="width: 100%;overflow-x: auto;white-space: nowrap;" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Record Type</th>
													<th>Last Update</th>
													<th>Last Modify By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

	<?php
		$this->load->view('foot');
	?>



	



		<div class ="modal" id = 'modal_edit_rates' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-md">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title"> <span class="fa fa-money"></span> Edit Rates</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="eformOccupation" >

                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_e' role="alert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<center><strong> Changed successfully!</strong></center>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Fill empty field!</center></strong>
                  	</div>

                  	<div id="prof_target_rates"></div><br>
                  	<div id="student_target_rates"></div>
	                 
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="saveRates">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var base_url = "<?php echo base_url(); ?>";
			var ratelist  = $('#ratelist').DataTable( {
				"processing": false,
	    		"ajax": {
	        		"url" : base_url +"setting/loadRates"
	     		 },
	     		 "ordering": false,
	     		 "bFilter": false,
        		 "bInfo": false
			});

			var id = 0;
			$("#ratelist").on('click', '.edit', function(){
            	 id = $(this).attr('id');
            	
            	var prof_target_rates = $('div#prof_target_rates').html('');
            	var student_target_rates = $('div#student_target_rates').html('');
            	$('#modal_edit_rates').modal(true);

            	
				
            	// $.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':profession}, function(data) {
                	
				

            		switch(id){

            			case '<?=utils::solo?>':
	                       $.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::professional?>'}, function(data) {
	                       	var result = ''; 

	                       	result = '<h4><strong>Professional Fee</strong></h4>';
	                        for(var i=0;i<data.length;i++)
		      				{

			                   result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[i].codename+'"> '+data[i].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[i].codename+'"  id="'+data[i].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[i].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';
							}
							prof_target_rates.html(result);
			                },'json');



	                       $.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::student?>'}, function(data) {
	                       	var result = ''; 
	                       	result = '<h4><strong>Student Fee</strong></h4>';
	                        for(var i=0;i<data.length;i++)
		      				{

			                   result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[i].codename+'"> '+data[i].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[i].codename+'"  id="'+data[i].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[i].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';
							}

						student_target_rates.html(result);
		                },'json');
	                    break;

	                    case '<?=utils::group?>':
	                       	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::professional?>'}, function(data) {
	                       		var result = ''; 
	                    		result = '<h4><strong>Professional Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	prof_target_rates.html(result);
	                       	},'json');


	                       	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::student?>'}, function(data) {
	                       		var result = ''; 
	                    	result = '<h4><strong>Student Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	student_target_rates.html(result);
	                       	},'json');
	                       	break;

	                     case '<?=utils::hall?>':

	                     	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::professional?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Professional Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	prof_target_rates.html(result);
	                       	},'json');

	                       	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::student?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Student Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	student_target_rates.html(result);
	                       	},'json');
	                     	break;

	                    case '<?=utils::weekly?>':
	                    	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::professional?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Professional Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	prof_target_rates.html(result);
	                       	},'json');

	                       	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::student?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Student Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	student_target_rates.html(result);
	                       	},'json');
	                    break;
	                    case '<?=utils::monthly?>':

	                    	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::professional?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Professional Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	prof_target_rates.html(result);
	                       	},'json');

	                       	$.post(base_url + "setting/getRatesDetail/",{'id':id,'profession':'<?=utils::student?>'}, function(data) {
	                       		var result = ''; 
	                       		
	                    		result = '<h4><strong>Student Fee</strong></h4>';
	                       		result +='<div class="item form-group">'+
			                      '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="'+data[0].codename+'"> '+data[0].rate_name+' <span class="required" style="color: red">*</span>'+
			                      '</label>'+
			                      '<div class="col-md-6 col-sm-6 col-xs-12">'+
			                        '<input name="'+data[0].codename+'"  id="'+data[0].codename+'"   class="form-control col-md-7 col-xs-12" type="number" value="'+data[0].rates_amount+'">'+
			                      '</div>'+
			                   '</div>';

	                       	student_target_rates.html(result);
	                       	},'json');

	                    break;
                    }         
 				});

				
				$("#saveRates").on('click',function(){
					

	                switch(id){
	                	case '<?=utils::solo?>': //1

	                		var p_oneday = $('#p_oneday').val();
							var p_promo = $('#p_promo').val();
							var p_onehour = $('#p_onehour').val();
							var p_twohour = $('#p_twohour').val();
							var s_oneday = $('#s_oneday').val();
							var s_promo = $('#s_promo').val();
							var s_onehour = $('#s_onehour').val();
							var s_twohour = $('#s_twohour').val();
							var p_vip = $('#p_vip').val();
							var s_vip = $('#s_vip').val();
							
							var input = {'p_oneday':p_oneday,'p_promo':p_promo,'p_onehour':p_onehour,'p_twohour':p_twohour,'s_oneday':s_oneday,'s_promo':s_promo,'s_onehour':s_onehour,'s_twohour':s_twohour,'p_vip':p_vip,'s_vip':s_vip}

							var notempty = 0; 

					        $.each( input, function( key, value ) {
					            if(isEmpty(value)){
					              notempty++;
					            }
					        });

					        if(Object.keys(input).length != notempty){
					        	$('#alert_danger_empty_e').removeClass('hide').show().delay(2000);
					        }else{
					        	$.post(base_url + "setting/saveRates/",{'id':id,'input':input}, function(data) {
			                    	$('#alert_success_e').removeClass('hide').show().delay(2000);
			                	},'json');
					        }

	                	break;
	                	case '<?=utils::hall?>': //2
	                		var s_p_hourly_rate = $('#s_p_hourly_rate').val();
	                		var s_s_hourly_rate = $('#s_s_hourly_rate').val();


	                		var input = {'s_p_hourly_rate':s_p_hourly_rate,'s_s_hourly_rate':s_s_hourly_rate};

	                		var notempty = 0; 

					        $.each( input, function( key, value ) {
					            if(isEmpty(value)){
					              notempty++;
					            }
					        });

					        if(Object.keys(input).length != notempty){
					        	$('#alert_danger_empty_e').removeClass('hide').show().delay(2000);
					        }else{
					        	$.post(base_url + "setting/saveRates/",{'id':id,'input':input}, function(data) {
			                    	$('#alert_success_e').removeClass('hide').show().delay(2000);
			                	},'json');
					        }
	                	break;
	                	case '<?=utils::group?>': //3
	                		var o_p_hourly_rate = $('#o_p_hourly_rate').val();
	                		var o_s_hourly_rate = $('#o_s_hourly_rate').val();


	                		var input = {'o_p_hourly_rate':o_p_hourly_rate,'o_s_hourly_rate':o_s_hourly_rate};

	                		var notempty = 0; 

					        $.each( input, function( key, value ) {
					            if(isEmpty(value)){
					              notempty++;
					            }
					        });

					        if(Object.keys(input).length != notempty){
					        	$('#alert_danger_empty_e').removeClass('hide').show().delay(2000);
					        }else{
					        	$.post(base_url + "setting/saveRates/",{'id':id,'input':input}, function(data) {
			                    	$('#alert_success_e').removeClass('hide').show().delay(2000);	
			                	},'json');
					        }
	                	break;
	                	case '<?=utils::weekly?>': //4
	                		var w_p_hourly_rate = $('#w_p_hourly_rate').val();
	                		var w_s_hourly_rate = $('#w_s_hourly_rate').val();


	                		var input = {'w_p_hourly_rate':w_p_hourly_rate,'w_s_hourly_rate':w_s_hourly_rate};

	                		var notempty = 0; 

					        $.each( input, function( key, value ) {
					            if(isEmpty(value)){
					              notempty++;
					            }
					        });

					        if(Object.keys(input).length != notempty){
					        	$('#alert_danger_empty_e').removeClass('hide').show().delay(2000);
					        }else{
					        	$.post(base_url + "setting/saveRates/",{'id':id,'input':input}, function(data) {
			                    	$('#alert_success_e').removeClass('hide').show().delay(2000);
			                	},'json');
					        }
	                	break;
	                	case '<?=utils::monthly?>': //5
	                		var m_p_hourly_rate = $('#m_p_hourly_rate').val();
	                		var m_s_hourly_rate = $('#m_s_hourly_rate').val();


	                		var input = {'m_p_hourly_rate':m_p_hourly_rate,'m_s_hourly_rate':m_s_hourly_rate};

	                		var notempty = 0; 

					        $.each( input, function( key, value ) {
					            if(isEmpty(value)){
					              notempty++;
					            }
					        });

					        if(Object.keys(input).length != notempty){
					        	$('#alert_danger_empty_e').removeClass('hide').show().delay(2000);
					        }else{
					        	$.post(base_url + "setting/saveRates/",{'id':id,'input':input}, function(data) {
			                    	$('#alert_success_e').removeClass('hide').show().delay(2000);
			                	},'json');
					        }
	                	break;
	                }

				});

			});
	</script>

	</body>
</html>
