<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>

	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
			 

			<?php
				$this->load->view('nav');
			?>

				<!-- page content -->
				<div class="right_col" role="main" >
					<div class="">
						<div class="row">
							<div class="col-md-12" style="width: 100%;overflow-x: auto;white-space: nowrap;">
								<div class="x_panel">
									<div class="x_title">
										<h2>Transaction Summary</h2>
										
											<div style="float: right">
                           
                      <h4>Total Amount Due <span id="amount">0.00</span></h4>
                      <h5>Overdue Amount <span id="overdueamount">0.00</span></h5>
                      </div>
										


										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="col-md-12 col-sm-12 col-xs-12">
											
											 <section class="content invoice">
                      <!-- title row -->
                      <div class="row">
                        <div class="col-xs-12 invoice-header">
                          <h3>
                                          <i class="fa fa-user"></i> Customer Details
                                          <small class="pull-right"></small>
                                      </h3>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          
                          <address>
                                          <strong>Name:</strong> <span id="name"> </span>
                                          <!-- <br><strong>Address:</strong> <span id="address"></span> -->
                                          <br><strong>Profession:</strong> <span id="occupation"></span>
                                          <br><strong>Phone:</strong> <span id="contactnumber"></span>
                                          <br><strong>Email:</strong> <span id="email"></span>
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          
                          <address>
                                          <strong>Entry Code: </strong><span id="entrycode"></span>
                                          <br><strong>Record Type: </strong><span id="recordtype"></span>
                                          <br><strong>Status: </strong> <span id="status"></span>
                                      </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          <strong>Hours: </strong><span id="hours"></span>
                          <br>
                          <strong>Time In: </strong><span id="timein"></span>
                          <br>
                          <strong>Time Out: </strong><span id="timeout"></span>
                          <br>
                          <strong>Time Remaining: </strong><span id="timeremaining"></span>
                          <br>
                          <strong>Time Overdue: </strong><span id="timeoverdue"></span>
                          <br>
                          <strong>Time Covered: </strong><span id="timecovered"></span>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- Table row -->
                      <div class="row">
                        <!-- <div class="col-xs-12 invoice-header"> -->
                          <h3>
                                <i class="fa fa-history"></i> History
                                <small class="pull-right"></small>
                            </h3>
                        <!-- </div> -->
                        <div class="col-xs-12 table">
                          <table class="table table-striped table-bordered" style="width: 100%;overflow-x: auto;white-space: nowrap;" id="transactionHistory">
                            <thead>
                              <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Modify by</th>
                              </tr>
                            </thead>
                            <tbody id='data_history'>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <div class="row">
                        <!-- accepted payments column -->
                        <!-- <div class="col-xs-6">
                         <p class="lead">Amount Due </p>
                          <div class="table-responsive">
                            <table class="table">
                              <thead>
                                <tr>
                                  <td>Total:</td>
                                  <td><span id="amount">0.00</span></td>
                                </tr>
                              </thead>
                            </table>
                          </div>
                        </div> -->
                        <!-- /.col -->
<!--                         <div class="col-xs-12">
                          <div id="memberColumn">
                            
                             
                          <h3>
                                <i class="fa fa-users"></i> Members
                                <small class="pull-right"></small>
                            </h3>
           
                              <div class="table-responsive">
                                <table class="table table-striped table-bordered dt-responsive nowrap" id="tableMember">
                                  <thead>
                                    <tr>
                                      <th>Name</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody id="data_member">
                                    
                                  </tbody>
                                </table>
                              </div>

                          </div>
                        </div> -->
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->

                      <!-- this row will not appear when printing  data-toggle="modal" data-target="#settlepayment"-->
                      <div class="row no-print">

                        <div class="col-xs-12">
                           <hr>
                          <button class="btn btn-success pull-right"  style="margin-right: 5px;" id="settled"><i class="fa fa-check" ></i> Settled</button>
                          <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#paymentadjustment" style="margin-right: 5px;" id="adjustpayment"><i class="fa fa-money" ></i> Discount</button>
                        </div>
                      </div>
                    </section>
										</div>
									</div>
								</div>
							</div>
						</div>



						


						
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

    <div class ="modal" id = 'settlepayment' tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><span class="fa fa-pencil"></span> Settle Payment</h4>
          </div>
          <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left"   >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong><center>Password changed successfully!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Fill empty field!</center></strong>
                    </div>

                   

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Amount <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="totalamount" id="totalamount"  class="form-control col-md-7 col-xs-12" type="number">
                          </div>
                      </div>

                  </div>
                
              </div>
            </div>



              
            

          </div>
          <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
                <!-- <button type="button" class="btn btn-success" id="settled"> <span class="fa fa-money"></span> Settle Payment</button> -->
                </form>
          </div>
        </div>
      </div>
  </div>

      <div class ="modal" id = 'paymentadjustment' tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><span class="fa fa-pencil"></span> Adjust Payment</h4>
          </div>
          <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left"   >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success_add_time' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong><center>Adjust payment successfully</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_extend' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Fill empty field!</center></strong>
                    </div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Discounted Amount <span class="required" style="color:red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="adjustamount" id="adjustamount"  class="form-control col-md-7 col-xs-12" type="number">
                          </div>
                      </div>

                  </div>
                
              </div>
            </div>
          </div>

          <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
                <button type="button" class="btn btn-success" id="adjustpaymentamount"> </span> Submit</button>
                </form>
          </div>
        </div>
      </div>
  </div>
	
		<?php
			$this->load->view('foot');
		?>
		<script type="text/javascript">



      $(document).ready(function() {

        
      var base_url = "<?php echo base_url(); ?>";
      var url = window.location.pathname;
      var id = url.substring(url.lastIndexOf('/') + 1);

      var role = "<?=$this->session->userdata('role')?>"
     
      if(role == 3){
        $('#adjustpayment').hide();
      }
      details(base_url,id);

      var transactionHistory  = $('#transactionHistory').DataTable( {
          "processing": false,
          "searching": false,
            "ajax": {
                "url" : base_url +"transaction/getTransactionHistory/"+id
             },
          "ordering": false
      });

      // var tableMember  = $('#tableMember').DataTable( {
      //     "processing": false,
      //     "searching": false,
      //       "ajax": {
      //           "url" : base_url +"transaction/getMembers/"+id
      //        },
      //     "ordering": false
      // });

      

      


      




      // $("#tableMember").on('click', '.out', function(){
      //     var member_id = $(this).attr('id');
      //     var status = {'status':<?=utils::inactive?>};
          

      //     $.confirm({
      //         title: 'Confirmation ',
      //         // content: 'Simple confirm!',
      //         buttons: {
      //         confirm: function () {
      //             $.post(base_url + "transaction/memberStatus/"+member_id,{'status':status}, function(data) {
      //                 transactionHistory.ajax.reload(null,false);
      //                 tableMember.ajax.reload(null,false);
      //                 details(base_url,id);
      //             },'json');
      //         },
      //         cancel: function () {
      //             // $.alert('Canceled!');
      //         }, 
      //         }
      //     });

      // });

      // $("#tableMember").on('click', '.in', function(){
      //     var member_id = $(this).attr('id');
      //     var status = {'status':<?=utils::active?>};

      //     $.post(base_url + "transaction/memberStatus/"+member_id,{'status':status}, function(data) {
      //         tableMember.ajax.reload(null,false);
      //     },'json');
      // });


        $("#settled").on('click',function(){
            // var amount = $('#totalamount').val();

             

             $.confirm({
              title: 'Confirmation ',
              // content: 'Simple confirm!',
              buttons: {
              confirm: function () {
                  $.post(base_url + "transaction/settleTransaction/"+id, function(data) {
                      $('#settled').hide();
                      $('#adjustpayment').hide();
                      details(base_url,id);
                      transactionHistory.ajax.reload(null,false);
                      // tableMember.ajax.reload(null,false);
                  },'json');
              },
              cancel: function () {
                  // $.alert('Canceled!');
              }, 
              }
              });
  

            // if(!isEmpty(amount)){
            //   $('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
            // }else{
             

            //   $("#settlepayment").modal('toggle');
            // }
        });


        $("#adjustpaymentamount").on('click',function(){
            var amount = $('#adjustamount').val();


            $.confirm({
              title: 'Confirmation ',
              // content: 'Simple confirm!',
              buttons: {
              confirm: function () {
                  if(!isEmpty(amount)){
                    $('#alert_danger_empty_extend').removeClass('hide').show().delay(2000).fadeOut();
                  }else{
                      $.post(base_url + "transaction/adjustPayment/"+id, {'amount':amount}, function(data) {
                        details(base_url,id);
                        transactionHistory.ajax.reload(null,false);
                        details(base_url,id);
                        $('#adjustamount').val('');
                      },'json');
                     
                    $("#paymentadjustment").modal('toggle');
                  }
              },
              cancel: function () {
                  // $.alert('Canceled!');
              }, 
              }
              });
            
            
        });

      });

      function details(base_url,id){
            $.get(base_url + "transaction/getTransactionDetails/"+id, function(data) {
              $('span#name').html(data.name);
              // $('span#address').html(data.address);
              $('span#occupation').html(data.occupation);
              $('span#contactnumber').html(data.contact_number);
              $('span#email').html(data.email);
              $('span#entrycode').html(data.pass_code);
              $('span#hours').html(data.hours);
              $('span#timein').html(data.verify_date);
              $('span#timeout').html(data.time_out);
              $('span#timeremaining').html(data.time_remaining);
              $('span#timeoverdue').html(data.time_overdue);
              $('span#recordtype').html(data.record_type);
              $('span#status').html(data.transaction_status);
              $('span#amount').html(data.amount);
              $('span#timecovered').html(data.time_covered);
              $('span#overdueamount').html(data.overdue_amount);

              if(data.record_type_int != <?=utils::group?>){
                  $("#memberColumn").hide();
              }

              if(data.status == <?=utils::transaction_done?>){
                $('#adjustpayment').hide();
                $('#settled').hide();
              }

            },'json');
      }

      // function members(id,base_url){
      //   $.post(base_url + "transaction/getMembers/"+id, function(data) {
            
      //   var data_member = $('tbody#data_member').html('');
      //      var result = '';
      //      for(var i=0;i<data.length;i++)
      //       {
      //         result += '<tr>'+
      //                     '<td>'+data[i].name+'</td>'+
      //                     '<td>'+data[i].status+'</td>'+
      //                   '</tr>'; 
      //       }

      //        data_member.html(result);

      //  },'json');
      // }

      // function transactionHistory(id,base_url){
      //   $.get(base_url + "transaction/getTransactionHistory/"+id, function(data) {
            
      //   var data_history = $('tbody#data_history').html('');
      //      var result = '';
      //      for(var i=0;i<data.length;i++)
      //       {
      //         result += '<tr>'+
      //                     '<td>'+data[i].date+'</td>'+
      //                     '<td>'+data[i].process+'</td>'+
      //                     '<td>'+data[i].name+'</td>'+
      //                   '</tr>'; 
      //       }

      //        data_history.html(result);

      // },'json');
      // }

      // function getTotalAmountPaid(id,base_url){
      //   $.get(base_url + "transaction/getTotalAmountPaid/"+id, function(data) {
            
      //     $('span#amount').html(data);

      //    },'json');
      // }
		</script>
	</body>
</html>
