<!DOCTYPE html>
<html lang="en">
  
  <?php
      $this->load->view('head');
  ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php
    $this->load->view('nav');
  ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small><span class="fa fa-user"></span> Enter your identification</small></h2>
                    <ul class="nav navbar-right panel_toolbox">

                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                        <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_auth' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Authentication does not exist or already expired!</center></strong>
                        </div>

                        <div class="alert alert-success alert-dismissible fade in hide" id='alert_success_auth' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center> Welcome back to OrangeDesk!</center></strong>
                        </div>

                        <form class="form-horizontal form-label-left"   id="form_code">
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> Email<span class="required" style='color: red'> *</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="email" class="form-control col-md-5 col-xs-12"  name="email"   type="text">
                            </div>
                          </div>
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"> Code<span class="required" style='color: red'> *</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="code" class="form-control col-md-5 col-xs-12"  name="code"   type="password">
                            </div>
                          </div>
                          <div class="form-group">
                        <div class="col-md-12 col-md-offset-3">
                          <button type="button" class="btn btn-success" id="validateCode">Submit</button>

                        </div>
                      </div>
                       </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            OrangeDesk Coworking Space And Study Lounge
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php
      $this->load->view('foot');
    ?>

	 <script type="text/javascript">

    $(document).ready(function() {
      var base_url = "<?php echo base_url(); ?>";
       $("#validateCode").on('click',function(){
          var email = $('#email').val();
          var code = $('#code').val();

          $.post(base_url + "registration/validateCode/",{'email':email,'code':code}, function(data) {
                if(data){
                  $('#alert_success_auth').removeClass('hide').show().delay(2000).fadeOut();
                  document.getElementById("form_code").reset();
                }else{
                  $('#alert_danger_invalid_auth').removeClass('hide').show().delay(2000).fadeOut();
                }
          },'json');

        });
    });

   </script>

  </body>
</html>