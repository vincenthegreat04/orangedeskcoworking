<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>

	<body class="login" style="overflow:hidden;background-color:white">
		<div>
			<div class="login_wrapper ">
				<div class="animate form login_form ">
					<section class="login_content">
						<form id="loginForm">
							<div class="alert alert-danger alert-dismissible fade in hide" id='alertdanger_account_notexist' role="alert">
			                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			                    </button>
			                   <strong>Email or password is incorrect!</strong>
		                  	</div>
							<h2>Login Form</h2>
							<div>
								<input type="text" class="form-control" placeholder="Email" name="email"  />
							</div>
							<div>
								<input type="password" class="form-control" placeholder="Password" name = "password" id="loginpassword" />
							</div>
							<div>
								<label style="float:right;"><input type="checkbox" onclick="showPassword()"  />Show password</label>
							</div>
							<br>
							<br>
							<div>

								<a class="btn btn-primary submit" id='login'>Log in</a>
								<a class="reset_pass" href="<?=base_url().'registration'?>">Registration</a>
							</div>

							<div class="clearfix"></div>

							<div class="separator">
								<div class="clearfix"></div>

								<div>
									<p>©2019 All Rights Reserved</p>
									<p> OrangeDesk Coworking Space And Study Lounge</p>
									<p> Privacy and Terms</p>
								</div>
							</div>
						</form>
					</section>
				</div>

				
			</div>
		</div>
	</body>

	<?php
		$this->load->view('foot');
	?>

	<script type="text/javascript">
		$(document).ready(function() {

			var base_url = "<?php echo base_url(); ?>";

			$("#login").on('click',function(){
				var form = $('#loginForm').serializeArray();

				$.post(base_url + "login/login", form, function(data) {
			
					if(data.exists == <?=utils::exists?>){
						window.location.href = "<?php echo base_url();?>home";
					}else{
						$('#alertdanger_account_notexist').removeClass('hide').show().delay(2000).fadeOut();
					}
				},'json');
			});
		});

	function showPassword() {
		  var x = document.getElementById("loginpassword");
		  if (x.type === "password") {
		    x.type = "text";
		  } else {
		    x.type = "password";
		  }
	}
	</script>
</html>
