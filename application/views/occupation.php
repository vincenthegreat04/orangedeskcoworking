<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				
	<?php
		$this->load->view('nav');
	?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2><small><span class="fa fa-briefcase"></span> Occupation List</small></h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
											<li class="dropdown">
												<a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#addoccupation"><i class="fa fa-plus"></i></a>
											</li> 
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										
										<table id="occupationList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Occupation Name</th>
													<th>Last Update</th>
													<th>Last Modify By</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

	<?php
		$this->load->view('foot');
	?>



	<div class ="modal" id = 'addoccupation' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-md">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title"><span class="fa fa-briefcase"></span> Add Occupation</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="formOccupation" >

                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success' role="alert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<center><strong> Added successfully!</strong></center>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center> Fill empty field!</center></strong>
                  	</div>


                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Occupation <span class="required" style="color: red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="occupation"  id="occupation"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="createOccupation">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>



		<div class ="modal" id = 'editoccupation' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-md">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title"> <span class="fa fa-briefcase"></span> Edit Occupation</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="eformOccupation" >

                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_e' role="alert">
	                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<center><strong> Changed successfully!</strong></center>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Fill empty field!</center></strong>
                  	</div>


                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="eoccupation">Occupation <span class="required" style="color: red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="eoccupation"  id="eoccupation"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="saveOccupation">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var base_url = "<?php echo base_url(); ?>";
			var occupationList  = $('#occupationList').DataTable( {
				"processing": false,
	    		"ajax": {
	        		"url" : base_url +"setting/loadOccupation"
	     		 },
	     		 "ordering": false
			});

			$("#createOccupation").on('click',function(){

				var occupation = $('#occupation').val();

				if(isEmpty(occupation)){
					$.post(base_url + "setting/addOccupation/", {'occupation':occupation}, function(data) {
	            		if(data){
	            			document.getElementById("formOccupation").reset();
	            			occupationList.ajax.reload(null,false);
	            			$('#alert_success').removeClass('hide').show().delay(2000).fadeOut();
	            		}
            		},'json');
				}else{
					$('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
				}
		  				
			});

			var id = 0;
			$("#occupationList").on('click', '.edit', function(){
            	 id = $(this).attr('id');
            	$('#editoccupation').modal(true);
            	$.post(base_url + "setting/getOccupation/"+id, function(data) {
            		 $('#eoccupation').val(data);
            	},'json');
            });

            

            $("#saveOccupation").on('click',function(){

				var occupation = $('#eoccupation').val();
			
				if(isEmpty(occupation)){
					$.post(base_url + "setting/saveOccupation/", {'occupation':occupation,'id':id}, function(data) {
	            		if(data){
	            			$('#alert_success_e').removeClass('hide').show().delay(2000).fadeOut();
	            			occupationList.ajax.reload(null,false);
	            		}
            		},'json');
				}else{
					$('#alert_danger_empty_e').removeClass('hide').show().delay(2000).fadeOut();
				}
		  				
			});
			

			});
	</script>

	</body>
</html>
