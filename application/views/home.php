<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>

	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
			 

			<?php
				$this->load->view('nav');
			?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="row top_tiles">
							<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: orange">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-users" style="color: orange"></i></div>
									<div class="count activecustomer">0</div>
									<h4>Today total customers</h4>
								</div>
							</div>
							<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: red">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-clock-o" style="color: red"></i></div>
									<div class="count overduecustomer">0</div>
									<h4>Today overdue customers</h4>
								</div>
							</div>
							<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: green">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-check-square-o" style="color: green"></i></div>
									<div class="count donecustomer">0</div>
									<h4>Today paid customers</h4>
								</div>
							</div>
							<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" style="color: blue">
								<div class="tile-stats">
									<div class="icon"><i class="fa fa-money" style="color: blue"></i></div>
									<div class="count totalearn">0</div>
									<h4>Today total income</h4>
								</div>
							</div>
						</div>

						<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12" >
								<div class="x_panel">
									<div class="x_title">
										<h2> <span class="fa fa-clock-o"></span> Transaction  </h2>
										<div class="filter">
											<div class="control-group pull-right">
				                              <div class="controls">
				                                <div class="input-prepend input-group">
				                                 <input type="text" name="reservation-time" id="reservation-time" class="form-control" value="<?=$this->utils->getAdjustedDateTime()?>" />
				                                   <span class="add-on input-group-addon" id="datefilter"><i class="glyphicon glyphicon-calendar fa fa-search"></i></span>
				                                </div>
				                              </div>
				                            </div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<ul class="nav navbar-right panel_toolbox">
														<li class="dropdown">
												<button  data-toggle="modal" data-target="#verifyCustomer" class="btn btn-success"><i class="fa fa-check"></i> Verify Code</button>
											</li>
														
													</ul>
													<!--  -->
											<table id="transactionList" class="table table-striped table-bordered" style="width: 100%;overflow-x: auto;white-space: nowrap;" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Entry ID</th>
													<th>Name</th>
													<th>Record Type</th>
													<th>Registered</th>
													<th>Remaining</th>
													<th>Overdue</th>
													<th>Covered</th>
													<th>Status</th>
													<th>Remove</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>

										</div>



									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="x_panel">
									<div class="x_title">
										<h2> <span class="fa fa-users"></span> Today new customers</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										
		                                <div id="data_newcustomer">
		                                	
		                                </div>

									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="x_panel">
									<div class="x_title">
										<h2> <span class="fa fa-money"></span> Today highly paid </h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
											
											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										 <div id="data_highpaid">
		                                	
		                                </div>
									</div>
								</div>
							</div>

							
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

			<div class ="modal" id = 'verifyCustomer' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-md">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title">Verify Code</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="verifyCustomerForm" >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<strong><center>Code Verified!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Fill empty field!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Invalid code or Already validated</center></strong>
                  	</div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Verification Code <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="code" id="code"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>

                        <!-- <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"><span class="required"></span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="checkbox" onclick="showPassword()">Show Verification Code
                          </div>
                        </div> -->

                        
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="verifyCode">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>
	
		<?php
			$this->load->view('foot');
		?>
		<script type="text/javascript">
			$(document).ready(function() {
				var base_url = "<?php echo base_url();?>";

				var datetime  = $('#reservation-time').val();

				// alert(datetime)
			    var arr = datetime.split('-');
			
			    var startdate = getDate(arr[0].trim());
				var enddate = getDate(arr[1].trim());

				var start = startdate.split(' ');
				var start_date = start[0];
				var start_time = start[1];

				var end = enddate.split(' ');
				var end_date = end[0];
				var end_time = end[1];

				details(base_url);
				var transactionList  = $('#transactionList').DataTable( {
					"processing": false,
					"searching": false,
		    		"ajax": {
		        		"url" : base_url +"transaction/loadTransaction/"+start_date+"/"+start_time+"/"+end_date+"/"+end_time
		     		 },
		     		 "ordering":false,
		     		 "searching": true,
		     		
				});

				$("#verifyCode").on('click',function(){
			  		var code  = $('#code').val();

			  		switch(true)
			        {
			          case isEmpty(code) == false:
			            $('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
			            break;
			          
			          default:
			          	$.post(base_url + "transaction/verifyCode", {'code':code}, function(data) {
			          		if(data){
			          			// $('#alert_success').removeClass('hide').show().delay(2000).fadeOut();
			          			document.getElementById("verifyCustomerForm").reset();
			          			$("#verifyCustomer").modal('toggle');
			          			details(base_url);
		              			transactionList.ajax.reload(null,false);
			          		}else{
			          			$('#alert_danger_invalid').removeClass('hide').show().delay(2000).fadeOut();
			          		}
			          	},'json');
			          	break;
			         }

		  		});

           		$('#datefilter').on('click',function(){
				 var datetime  = $('#reservation-time').val();
			     var arr = datetime.split('-');
			
			     var startdate = getDate(arr[0].trim());
				 var enddate = getDate(arr[1].trim());

				 var start = startdate.split(' ');
				 var start_date = start[0];
				 var start_time = start[1];

				 var end = enddate.split(' ');
				 var end_date = end[0];
				 var end_time = end[1];

				 transactionList.ajax.url( base_url +"transaction/loadTransaction/"+start_date+"/"+start_time+"/"+end_date+"/"+end_time).load();
				
			}	);

           	$('#transactionList').on('click', '.remove', function(){
				var id = $(this).attr('id');


			  $.confirm({
              title: 'Confirmation ',
              buttons: {
              confirm: function () {
                 $.post(base_url + "transaction/removeTransaction/", {'id':id}, function(data) {
	            	if(data){
	            		details(base_url);
		              	transactionList.ajax.reload(null,false);
	            	}
		       		},'json');
              },
              cancel: function () {
                 	
              }, 
              }
              });

				

			});


			});

			

		

			function details(base_url){

				$.post(base_url + "home/getActiveMember/", function(data) {
	            	var activecustomer = $('div.activecustomer').html('');
	            	activecustomer.html(data);
		       },'json');

				$.post(base_url + "home/getOverdueCustomer/", function(data) {
	            	var overduecustomer = $('div.overduecustomer').html('');
	            	overduecustomer.html(data);
		       },'json');

				$.post(base_url + "home/getDoneCustomer/", function(data) {
	            	var donecustomer = $('div.donecustomer').html('');
	            	donecustomer.html(data);
		       },'json');

				$.post(base_url + "home/getEarnToday/", function(data) {
	            	var totalearn = $('div.totalearn').html('');
	            	totalearn.html(data);
		       },'json');


				$.post(base_url + "home/getNewCustomer/", function(data) {
                  	
				   var data_newcustomer = $('div#data_newcustomer').html('');
		           var result = '';
		           for(var i=0;i<data.length;i++)
		            {
		            	
		              result += '<article class="media event">'+
											'<a class="pull-left date">'+
												'<p class="month">'+data[i].month+'</p>'+
												'<p class="day">'+data[i].day+'</p>'+
											'</a>'+
											'<div class="media-body">'+
												'<a class="title" href='+base_url+"transaction/detail/"+data[i].id+'>'+data[i].pass_code+'</a>'+
												'<p>'+data[i].name+' in at '+data[i].verify_date+' </p>'+
											'</div>'+
										'</article>';

		            }

             		data_newcustomer.html(result);

           		},'json');


           		$.post(base_url + "home/getHighPaid/", function(data) {
                  	
				   var data_highpaid = $('div#data_highpaid').html('');
		           var result = '';
		           for(var i=0;i<data.length;i++)
		            {
		             
		            	result += '<article class="media event">'+
											'<a class="pull-left date">'+
												'<p class="month">'+data[i].month+'</p>'+
												'<p class="day">'+data[i].day+'</p>'+
											'</a>'+
											'<div class="media-body">'+
												'<a class="title" href='+base_url+"transaction/detail/"+data[i].id+'>'+data[i].pass_code+'</a>'+
												'<p>'+data[i].name+' paid  '+data[i].amount+' for '+data[i].hours+ ' at '+data[i].settled_date+'</p>'+
											'</div>'+
										'</article>';
		            }

             		data_highpaid.html(result);

           		},'json');
			}
			

		function showPassword() {
		  var x = document.getElementById("code");
		  if (x.type === "password") {
		    x.type = "text";
		  } else {
		    x.type = "password";
		  }
		}

		</script>
	</body>
</html>
