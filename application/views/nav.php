 <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-book"></i> <span>OrangeDesk</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url().'assets/production/images/user.png'?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>

                  <?php
                    if($this->session->userdata('logged_in')){
                      ?>
                       <h2><?=$this->session->userdata('name')?></h2>
                      <?php
                    }else{
                      ?>
                        <h2>Customers</h2>
                      <?php
                    }
                  ?>
              </div>
            </div>
            

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
              <?php
                if($this->session->userdata('logged_in')){
              ?>
               <li><a href="<?php echo base_url().'home/'?>"><i class="fa fa-home"></i> Home</a></li>
               <li><a href="<?php echo base_url().'transaction/'?>"><i class="fa fa-money"></i> Income</a></li>
        
                    <li><a><i class="fa fa-gears"></i> System Setting <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                          <?php
                            if( $this->session->userdata('role') != utils::receptionist  ){
                          ?>
                          <li><?php echo anchor('setting/user', 'Users', 'title="Users"'); ?></li>
                          <li><?php echo anchor('setting/rates', 'Rates', 'title="Rates"'); ?></li>
                          <?php
                           }
                          ?>
                          <li><?php echo anchor('setting/freecode', 'Free pass', 'title="Free pass"'); ?></li>
                      </ul>
                    </li>
                  <?php
               }
               
            if($this->session->userdata('logged_in') != true){
              ?>
                <li ><a href="<?php echo base_url().'registration/'?>"><i class="fa fa-clock-o"></i> Registration</a></li>
                <li ><a href="<?php echo base_url().'registration/code'?>"><i class="fa fa-pencil"></i> Code</a></li>

              <?php
            }
          ?>

                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>
        

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">

              <?php
              if($this->session->userdata('role') == true){
              ?>

                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url().'assets/production/images/user.png'?>" alt=""> <?=$this->session->userdata('name')?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="#" data-toggle="modal" data-target="#viewprofile" > Change Password</a></li>
                    <li><a href="<?php echo base_url().'login/logout'?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge bg-orange" id="currentlyoverdue">0</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <div id="data_notification">
                      
                    </div>

                  </ul>
                </li>
                <?php
                  }else{
                    ?>
                      <li class="">
                        <a href="<?php echo base_url().'login'?>" class="user-profile dropdown-toggle">
                          Login
                        </a>
                      </li>
                      <!-- <li class="">
                        <a href="#" data-toggle="modal" data-target="#passcode" class="user-profile dropdown-toggle">
                         
                        </a>
                      </li> -->
                    <?php
                  }
                ?>

                  

              </ul>
            </nav>
          </div>
        </div>




    <div class ="modal" id = 'passcode' tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><span class="fa fa-pencil"></span> Extend Time</h4>
          </div>
          <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left"  id="extendtimeform" >

                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_extend_time' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong><center>Extend time successfully</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_customer_code' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Fill empty field!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_time_customer_code' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Invalid hours!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_customer_code' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Invalid code entered or Unverified Code or Expired Code</center></strong>
                    </div>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="customercode">Verification Code <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="customercode" id="customercode"   class="form-control col-md-7 col-xs-12" type="password">
                        </div>
                    </div>

                    <div class="item form-group hide extendtime">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hoursextend">Hours <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="hoursextend" id="hoursextend"   class="form-control col-md-7 col-xs-12" type="number">
                        </div>
                    </div>
                    <div class="item form-group hide extendtime">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hoursextend">Amount <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="amountextend" id="amountextend"   class="form-control col-md-7 col-xs-12" type="number">
                        </div>
                    </div>

                      
                     

                  </div>
                
              </div>
            </div>
              
            

          </div>
          <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
                <button type="button" class="btn btn-success" id="addTimeExtend">Extend</button>
                <button type="button" class="btn btn-success" id="customerCodeVerification">Save</button>
                </form>
          </div>
        </div>
      </div>
  </div>





    <div class ="modal" id = 'viewprofile' tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><span class="fa fa-pencil"></span> Change Password</h4>
          </div>
          <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left"  id="changepassword" >

                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_profile' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong><center>Password changed successfully!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_profile' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Fill empty fields!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_profile' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Invalid old password!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_mismatched_profile' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Mismatched new password!</center></strong>
                    </div>

                     

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="oldpassword">Old Password 
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="oldpassword" id="oldpassword"   class="form-control col-md-7 col-xs-12" type="password">
                          </div>
                      </div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="newpassword">New Password 
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="newpassword" id="newpassword"   class="form-control col-md-7 col-xs-12" type="password">
                          </div>
                      </div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="verifynewpassword">Verify New Password 
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="verifynewpassword" id="verifynewpassword"   class="form-control col-md-7 col-xs-12" type="password">
                          </div>
                      </div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="checkbox" onclick="thePasswordShow()"  />Show passwords
                          </div>
                      </div>

                      

                  </div>
                
              </div>
            </div>
              
            

          </div>
          <div class="modal-footer">
          <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
                <button type="button" class="btn btn-success" id="saveprofie">Save</button>
                </form>
          </div>
        </div>
      </div>
  </div>
        <!-- /top navigation -->
