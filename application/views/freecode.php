<!DOCTYPE html>
<html lang="en">
  <?php
    $this->load->view('head');
  ?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        
  <?php
    $this->load->view('nav');
  ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><span class="fa fa-pencil"></span> Free Code List</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>

                      <?php
                        if($this->session->userdata('role') != utils::receptionist){
                          ?>
                            <li class="dropdown">
                            <a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#addfreecode"><i class="fa fa-plus"></i></a>
                          </li> 
                          <?php
                        }
                      ?>
                          
                   
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_defaultpassword' role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong><center>Successfully changed password to default!</center></strong>
                              </div>
          
                    <table id="freecodeList" class="table table-striped table-bordered cellspacing="0" width="100%" style="width: 100%;overflow-x: auto;white-space: nowrap;">
                      <thead>
                        <tr>
                          <th>Code</th>
                          <th>Hours</th>
                          <th>Status</th>
                          <th>Last Updated</th>
                          <th>Last Modify By</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
          
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            OrangeDesk Coworking Space And Study Lounge
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

  <?php
    $this->load->view('foot');
  ?>



  <div class ="modal" id = 'addfreecode' tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title"><span class="fa fa-user"></span> Add new free pass code</h4>
          </div>
          <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left"  id="formFeeCode" >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                      <strong><center>New code successfully created!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Fill empty fields!</center></strong>
                    </div>

                    <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_exists' role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                      </button>
                     <strong><center>Code already exist</center></strong>
                    </div>
                        <div class="item form-group  no_hours_solo">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hours">Number Hours <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="hours" id="hours"  class="form-control col-md-7 col-xs-12">
                                 <option value="24">One day</option>
                                 <option value="4">Promo</option>
                                 <option value="1>">One Hour</option>
                                 <option value="2">Two Hours</option>
                                 <option value="3">Three Hours</option>
                                 <option value="7">Weekly</option>
                                 <option value="31">Monthly</option>
                            </select>
                            </div>
                          </div>
                          <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="code">Code <span class="required" style="color:red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="code"  id="code"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="generateRandomCode">Generate Code</button>
                <button type="button" class="btn btn-success" id="createFreeCode">Submit</button>
                </form>
          </div>
        </div>
      </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      var base_url = "<?php echo base_url(); ?>";
      var freecodeList  = $('#freecodeList').DataTable( {
        "processing": false,
          "ajax": {
              "url" : base_url +"setting/loadGeneratedCode"
           },
          "ordering":false,
          "bFilter": false,
          "bInfo": false
      });

      $('#generateRandomCode').on('click',function(){
          $('#code').val(makeid());
      });

      $('#createFreeCode').on('click',function(){
         var freecode = $('#code').val();
         var hours = $('#hours').val();

         // alert(hours)
         if(!isEmpty(freecode)){
              $('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
         }else{
            $.post(base_url + "setting/createFreeCode/",{'freecode':freecode,'hours':hours}, function(data) {
              if(data){
                  $('#alert_success').removeClass('hide').show().delay(2000).fadeOut();
                  freecodeList.ajax.reload(null,false);
                  document.getElementById("formFeeCode").reset();
              }else{
                $('#alert_danger_exists').removeClass('hide').show().delay(2000).fadeOut();
              }
             
            },'json');
         }
         

      });

    });

    function makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 7; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
  </script>

  </body>
</html>
