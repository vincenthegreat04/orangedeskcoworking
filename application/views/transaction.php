<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				
	<?php
		$this->load->view('nav');
	?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2><small>Income</small></h2>
										<div class="filter">
											<div class="control-group pull-right">
				                              <div class="controls">
				                                <div class="input-prepend input-group">
				                                 
				                                  <input type="text" name="reservation-time" id="reservation-time" class="form-control" value="<?=$this->utils->getAdjustedDateTime()?>" />
				                                   <span class="add-on input-group-addon" id="datefilter"><i class="glyphicon glyphicon-calendar fa fa-search"></i></span>
				                                  
				                                </div>
				                              </div> 
				                             
				                            </div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										
					
										<table id="recordList" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Record Type</th>
													<!-- <th>Time Registered</th>
													<th>Time Remaining</th>
													<th>Time Overdue</th> -->
													<!-- <th>Time Covered</th> -->
													<th>Income</th>
												</tr>
											</thead>
											<tbody id="data_income">
											</tbody>
											<tbody id="data_total">
											</tbody>
										</table>
					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>



	

	<?php
		$this->load->view('foot');
	?>

	<script type="text/javascript">
		$(document).ready(function() {
			var base_url = "<?php echo base_url(); ?>";



			var datetime  = $('#reservation-time').val();
			var arr = datetime.split('-');
			
			var startdate = getDate(arr[0].trim());
			var enddate = getDate(arr[1].trim());



			getIncome(base_url,startdate,enddate);
			

			$('#datefilter').on('click',function(){
				$('.abc').remove();
				  datetime  = $('#reservation-time').val();
			      arr = datetime.split('-');
			
			     startdate = getDate(arr[0].trim());
				 enddate = getDate(arr[1].trim());
				getIncome(base_url,startdate,enddate);
			});

			
			
		});

		function getIncome(base_url,startdate,enddate){
			$.post(base_url + "transaction/getIncome/", {record_type:'<?=utils::solo?>',startdate:startdate,enddate:enddate}, function(data) {
      			
			   $("tbody#data_income").append(
		   			'<tr class="abc" style="text-align:right">'+
		   				'<td style="text-align:left">Solo</td>'+
		   				// '<td>'+data.registered+'</td>'+
		   				// '<td>'+data.remaining+'</td>'+
		   				// '<td>'+data.overdue+'</td>'+
		   				// '<td>'+data.covered+'</td>'+
		   				'<td style="text-align:right">'+data.amount+'</td>'+
		   			'</tr>'
			   	);
				 
           	},'json');

           	$.post(base_url + "transaction/getIncome/", {record_type:'<?=utils::hall?>',startdate:startdate,enddate:enddate}, function(data) {
		          
				   $("tbody#data_income").append(
			   			'<tr class="abc" style="text-align:right">'+
			   				'<td style="text-align:left">Function Hall</td>'+
			   				// '<td>'+data.registered+'</td>'+
			   				// '<td>'+data.remaining+'</td>'+
			   				// '<td>'+data.overdue+'</td>'+
			   				// '<td>'+data.covered+'</td>'+
			   				'<td style="text-align:right">'+data.amount+'</td>'+
			   			'</tr>'
				   	);
				   
           	},'json');


           	$.post(base_url + "transaction/getIncome/", {record_type:'<?=utils::group?>',startdate:startdate,enddate:enddate}, function(data) {
		          
				   $("tbody#data_income").append(
			   			'<tr class="abc" style="text-align:right">'+
			   				'<td style="text-align:left">Group</td>'+
			   				// '<td>'+data.registered+'</td>'+
			   				// '<td>'+data.remaining+'</td>'+
			   				// '<td>'+data.overdue+'</td>'+
			   				// '<td>'+data.covered+'</td>'+
			   				'<td style="text-align:right">'+data.amount+'</td>'+
			   			 '</tr>'
				   	);
				  
           	},'json');

           	$.post(base_url + "transaction/getIncome/", {record_type:'<?=utils::weekly?>',startdate:startdate,enddate:enddate}, function(data) {
		          
				   $("tbody#data_income").append(
			   			'<tr class="abc" style="text-align:right">'+
			   				'<td style="text-align:left">Weekly Pass</td>'+
			   				// '<td>'+data.registered+'</td>'+
			   				// '<td>'+data.remaining+'</td>'+
			   				// '<td>'+data.overdue+'</td>'+
			   				// '<td></td>'+
			   				'<td style="text-align:right">'+data.amount+'</td>'+
			   			 '</tr>'
				   	);
				  
           	},'json');

           		$.post(base_url + "transaction/getIncome/", {record_type:'<?=utils::monthly?>',startdate:startdate,enddate:enddate}, function(data) {
		          
				   $("tbody#data_income").append(
			   			'<tr class="abc" style="text-align:right">'+
			   				'<td style="text-align:left">Monthly Pass</td>'+
			   				// '<td>'+data.registered+'</td>'+
			   				// '<td>'+data.remaining+'</td>'+
			   				// '<td>'+data.overdue+'</td>'+
			   				// '<td></td>'+
			   				'<td style="text-align:right">'+data.amount+'</td>'+
			   			 '</tr>'
				   	);
				  
           	},'json');


           	$.post(base_url + "transaction/getTotalIncome/",{startdate:startdate,enddate:enddate}, function(data) {
				   $("tbody#data_total").append(
		   			'<tr style="text-align:right" class="abc">'+
		   				'<td><strong>Total</strong></td>'+
		   				// '<td>'+data.registered+'</td>'+
		   				// '<td>'+data.remaining+'</td>'+
		   				// '<td>'+data.overdue+'</td>'+
		   				// '<td>'+data.covered+'</td>'+
		   				'<td >'+data.amount+'</td>'+
		   			'</tr>'
					);

           	},'json');
		}

		
	</script>

	</body>
</html>

