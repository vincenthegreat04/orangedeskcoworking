<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				
	<?php
		$this->load->view('nav');
	?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12" >
								<div class="x_panel">
									<div class="x_title">
										<h2><small><span class="fa fa-users"></span> Users List</small></h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>

											<?php 
												if($this->session->userdata('role') != utils::receptionist){
													?>
													<li class="dropdown">
														<a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#adduser"><i class="fa fa-plus"></i></a>
													</li> 
													<?php
												}
											?>
											
											
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="alert alert-warning alert-dismissible fade in hide" id='alert_success_defaultpassword' role="alert">
						                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
						                    </button>
					                    	<strong><center>Successfully changed password to default!</center></strong>
					                  	</div>
					
										<table id="userList" class="table table-striped table-bordered"  style="width: 100%;overflow-x: auto;white-space: nowrap;"cellspacing="0" width="100%">
											<thead>
												<tr>
													<th>Name</th>
													<th>Email</th>
													<th>Role</th>
													<th>Status</th>
													<th>Last Updated</th>
													<th>Last Modify By</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						OrangeDesk Coworking Space And Study Lounge
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

	<?php
		$this->load->view('foot');
	?>



	<div class ="modal" id = 'adduser' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title"><span class="fa fa-user"></span> Add New User</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="formCreateUser" >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<strong><center>New account successfully created!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Fill empty fields!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_mismatch' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Email not matched!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Invalid email!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_exists' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Email already exist</center></strong>
                  	</div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required" style="color:red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="name"  id="name"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>

                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email"  name="email" id="email" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Confirm Email <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" name="confirm_email" id="confirm_email" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">User Role <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select id="role" name="role" class="form-control col-md-7 col-xs-12">
                         	<option value="">Select Role</option>
                         	<option value="<?=utils::admin?>">Admin</option>
                         	<option value="<?=utils::receptionist?>">Receptionist</option>
                         </select>
                        </div>
                      </div>
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
	              <!-- <button class="btn btn-primary" type="button">Cancel</button> -->
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="createUser">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>


	<div class ="modal" id = 'edituser' tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">

	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	          </button>
	          <h4 class="modal-title"><span class="fa fa-user"></span> Edit User</h4>
	        </div>
	        <div class="modal-body">

             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" novalidate id="formUpdateUser" >

                    <div class="alert alert-success alert-dismissible fade in hide" id='alert_success_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
                    	<strong><center> Account successfully updated!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Fill empty fields!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Invalid email!</center></strong>
                  	</div>

                  	<div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_exists_e' role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	                    </button>
	                   <strong><center>Email already exist</center></strong>
                  	</div>

                      <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name_e">Name <span class="required" style="color:red">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="name_e"  id="name_e"   class="form-control col-md-7 col-xs-12" type="text">
                          </div>
                        </div>

                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email_e">Email <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email"  name="email_e" id="email_e" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                     
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">User Role <span class="required" style="color:red">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <select id="role_e" name="role_e" class="form-control col-md-7 col-xs-12">
                         	<option value="">Select Role</option>
                         	<option value="<?=utils::admin?>">Admin</option>
                         	<option value="<?=utils::receptionist?>">Receptionist</option>
                         </select>
                        </div>
                      </div>
                  </div>
                
              </div>
            </div>
	          	
	        	

	        </div>
	        <div class="modal-footer">
	              <!-- <button class="btn btn-primary" type="button">Cancel</button> -->
				  <!-- <button class="btn btn-primary" type="reset">Reset</button> -->
	              <button type="button" class="btn btn-success" id="updateUser">Submit</button>
	              </form>
	        </div>
	      </div>
	    </div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var base_url = "<?php echo base_url(); ?>";
			var userlist  = $('#userList').DataTable( {
				"processing": false,
	    		"ajax": {
	        		"url" : base_url +"setting/loadUser"
	     		 },
	     		"ordering":false,
	     		"bFilter": false,
        		"bInfo": false
			});

			$("#createUser").on('click',function(){
		  		var name  = $('#name').val();
		  		var confirm_email  = $('#confirm_email').val();
		  		var email  = $('#email').val();
		  		var role  = $('#role').val();

		  		var input = {'email':email,'confirm_email':confirm_email,'name':name,'role':role};
		  		var notempty = 0;

		  		$.each( input, function( key, value ) {
		            if(isEmpty(value)){
		              notempty++;
		            }
       			});

       			switch(true)
		        {
		          case Object.keys(input).length != notempty:
		            $('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
		            break;
		          case isValidEmailAddress(email) == false:
		            $('#alert_danger_invalid').removeClass('hide').show().delay(2000).fadeOut();
		            break;
		          case email != confirm_email:
		            $('#alert_danger_mismatch').removeClass('hide').show().delay(2000).fadeOut();
		            break;
		          default:
		          	$.post(base_url + "setting/addUser", {'input':input}, function(data) {
		          		if(data){
		          			$('#alert_success').removeClass('hide').show().delay(2000).fadeOut();
		          			document.getElementById("formCreateUser").reset();
	                		userlist.ajax.reload(null,false);
		          		}else{
		          			$('#alert_danger_exists').removeClass('hide').show().delay(2000).fadeOut();
		          		}
		          	},'json');
		          	break;
		         }

				 return false;				
			});

			$("#userList").on('click', '.deactivateUser', function(){
            	var user_id = $(this).attr('id');
            	
            	$.post(base_url + "setting/deactivateUser/"+user_id, function(data) {
            		if(data){
            			userlist.ajax.reload(null,false);
            		}
            	},'json');
            });

			$("#userList").on('click', '.activateUser', function(){
            	var user_id = $(this).attr('id');
            	
            	$.post(base_url + "setting/activateUser/"+user_id, function(data) {
            		if(data){
            			userlist.ajax.reload(null,false);
            		}
            	},'json');
            });
			
			$("#userList").on('click', '.default-password', function(){
            	var user_id = $(this).attr('id');
            	
            	$.post(base_url + "setting/defaultPassword/"+user_id, function(data) {
            		if(data){
            			$('#alert_success_defaultpassword').removeClass('hide').show().delay(2000).fadeOut();
            			userlist.ajax.reload(null,false);
            		}
            	},'json');
            });

			var user_id;
            $("#userList").on('click', '.profile', function(){
            	 user_id = $(this).attr('id');
            	$('#edituser').modal(true);
            	$.post(base_url + "setting/getProfileDetails/"+user_id, function(data) {
            		$('#name_e').val(data[0].name);
            		$('#email_e').val(data[0].email);
            		$('#role_e').val(data[0].role);
            	},'json');
            });

            
            $("#updateUser").on('click',function(){


		  		var name  = $('#name_e').val();
		  		var email  = $('#email_e').val();
		  		var role  = $('#role_e').val();

		  		var input = {'email':email,'name':name,'role':role};
		  		var notempty = 0;

		  		$.each( input, function( key, value ) {
		            if(isEmpty(value)){
		              notempty++;
		            }
       			});

       			switch(true)
		        {
		          case Object.keys(input).length != notempty:
		            $('#alert_danger_empty_e').removeClass('hide').show().delay(2000).fadeOut();
		            break;
		          case isValidEmailAddress(email) == false:
		            $('#alert_danger_invalid_e').removeClass('hide').show().delay(2000).fadeOut();
		            break;
		          default:
		          	$.post(base_url + "setting/updateUserDetail", {'input':input,'user_id':user_id}, function(data) {
		          		if(data){
		          			$('#alert_success_e').removeClass('hide').show().delay(2000).fadeOut();
		          			document.getElementById("formCreateUser").reset();
	                		userlist.ajax.reload(null,false);
		          		}else{
		          			$('#alert_danger_exists_e').removeClass('hide').show().delay(2000).fadeOut();
		          		}
		          	},'json');
		          	break;
		         }

				 return false;				
			});

			});
	</script>

	</body>
</html>
