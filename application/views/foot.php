<!-- jQuery -->
	<script src="<?php echo base_url().'assets/vendors/jquery/dist/jquery.min.js'?>"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url().'assets/vendors/bootstrap/dist/js/bootstrap.min.js'?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url().'assets/vendors/fastclick/lib/fastclick.js'?>"></script>
	<!-- NProgress -->
	<script src="<?php echo base_url().'assets/vendors/nprogress/nprogress.js'?>"></script>
	<!-- Chart.js -->
	<script src="<?php echo base_url().'assets/vendors/Chart.js/dist/Chart.min.js'?>"></script>
	<!-- jQuery Sparklines -->
	<script src="<?php echo base_url().'assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js'?>"></script>
	<!-- Flot -->
	<script src="<?php echo base_url().'assets/vendors/Flot/jquery.flot.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/Flot/jquery.flot.pie.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/Flot/jquery.flot.time.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/Flot/jquery.flot.stack.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/Flot/jquery.flot.resize.js'?>"></script>
	<!-- Flot plugins -->
	<script src="<?php echo base_url().'assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/flot-spline/js/jquery.flot.spline.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/flot.curvedlines/curvedLines.js'?>"></script>
	<!-- DateJS -->
	<script src="<?php echo base_url().'assets/vendors/DateJS/build/date.js'?>"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="<?php echo base_url().'assets/vendors/moment/min/moment.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/bootstrap-daterangepicker/daterangepicker.js'?>"></script>
	
	<!-- Custom Theme Scripts -->
	<!-- <script src="<?php echo base_url().'assets/build/js/custom.min.js'?>"></script> -->
	<script src="<?php echo base_url().'assets/build/js/custom.js'?>"></script>

	<!-- validator -->
    <script src="<?php echo base_url().'assets/vendors/validator/validator.js'?>"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url().'assets/vendors/iCheck/icheck.min.js'?>"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url().'assets/vendors/datatables.net/js/jquery.dataTables.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-buttons/js/buttons.flash.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-buttons/js/buttons.html5.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-buttons/js/buttons.print.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/jszip/dist/jszip.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/pdfmake/build/pdfmake.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/vendors/pdfmake/build/vfs_fonts.js'?>"></script>

	<!-- jQuery Smart Wizard -->
    <script src="<?php echo base_url().'assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'?>"></script>


 
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url().'assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'?>"></script>
   
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo base_url().'assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/vendors/jquery.hotkeys/jquery.hotkeys.js'?>"></script>
    <script src="<?php echo base_url().'assets/vendors/google-code-prettify/src/prettify.js'?>"></script>
    <!-- jQuery Tags Input -->
 	 <script src="<?php echo base_url().'assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js'?>"></script>
    <!-- Switchery -->
    <script src="<?php echo base_url().'assets/vendors/switchery/dist/switchery.min.js'?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url().'assets/vendors/select2/dist/js/select2.full.min.js'?>"></script>
    <!-- Parsley -->
    <!-- <script src="<?php echo base_url().'assets/vendors/parsleyjs/dist/parsley.min.js'?>"></script> -->
    <!-- Autosize -->
    <script src="<?php echo base_url().'assets/vendors/autosize/dist/autosize.min.js'?>"></script>
    <!-- jQuery autocomplete -->
    <script src="<?php echo base_url().'assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js'?>"></script>
    <!-- starrr -->
    <script src="<?php echo base_url().'assets/vendors/starrr/dist/starrr.js'?>"></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
	<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script> -->
<!-- 	
 -->
    <script type="text/javascript">


    	
    function isEmpty(x){
        return /\S+/.test(x)
    }

    function isValidEmailAddress(x) {
       var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    	return pattern.test(x);
    }

    function getDate(datetime){
			var datetime = datetime.split(' ');
			var date = datetime[0].split('/');
			var time = datetime[1].split(':');
			var hour = time[0]; var second = '00';
			if(datetime[2] == 'PM'){
				 hour = parseInt(hour) + 12;
				 second = '59';
			}else{
				if(hour == 12 ){
					hour = '00';
				}else if(hour < 10){
					hour = '0'+hour;
				}
			}
			var xyz = date[2]+'-'+date[0]+'-'+date[1]+' '+ hour+':'+time[1]+':'+second;
		
			return xyz;
	}

	function notification(base_url){

		   $.post(base_url + "home/getOverdue/", function(data) {
		   var data_notification = $('div#data_notification').html('');
	       var result = '';
	       for(var i=0;i<data.length;i++)
	       {
	       	 var id = data[i].transaction_id;
	          result += '<li>'+
                      '<a href="<?php echo base_url().'transaction/detail/'?>'+id+'">'+
                          '<span class="image"><img src="<?php echo base_url().'assets/production/images/user.png'?>" alt="Profile Image" /></span>'+
                          '<span>'+
                            '<span>'+data[i].name+'</span>'+
                          '</span>'+
                          '<span class="message">'+
                            'Overdue by '+data[i].time_overdue+
                          '</span>'+
                      '</a>'+
                      '</li>';
	        }
	 		data_notification.html(result);
			},'json');


		   $.post(base_url + "home/getTotalCurrentlyOverdue/", function(data) {
			    var currentlyoverdue = $('span#currentlyoverdue').html('');
		     	currentlyoverdue.html(data);
		   },'json');

	}


	function callTime(base_url){
    	// setInterval(function(){ notification(base_url); }, 1200000);
	}

	function thePasswordShow(){
		var oldpassword = document.getElementById("oldpassword");
		  if (oldpassword.type === "password") {
		    oldpassword.type = "text";
		  } else {
		    oldpassword.type = "password";
		  }

		var newpassword = document.getElementById("newpassword");
		  if (newpassword.type === "password") {
		    newpassword.type = "text";
		  } else {
		    newpassword.type = "password";
		 }

		 var verifynewpassword = document.getElementById("verifynewpassword");
		  if (verifynewpassword.type === "password") {
		    verifynewpassword.type = "text";
		  } else {
		    verifynewpassword.type = "password";
		 }
	}

	$(document).ready(function() {
		var base_url = "<?php echo base_url(); ?>";
		var isLogin = '<?=$this->session->userdata('logged_in')?>'

		$('#addTimeExtend').hide();
		notification(base_url);
		callTime(base_url);

		   var oldpassword = '';
		   if(isLogin){
		   		$.post(base_url + "setting/getPassword/", function(data) {
			    	oldpassword = data;
		   		},'json');
		   }


		  $("#customerCodeVerification").on('click',function(){
		  		var code = $('#customercode').val();

		  		if(!isEmpty(code)){
		  			$('#alert_danger_empty_customer_code').removeClass('hide').show().delay(2000).fadeOut();
		  		}else{
		  			$.post(base_url + "registration/getDetailByCode/",{'code':code}, function(data) {
		  				if(data){
		  					$('#customercode').attr("disabled", true);
		  					$('#customerCodeVerification').hide();
		  					$('#addTimeExtend').show();
		  					$('.extendtime').removeClass('hide').show()
		  				}else{
		  				$('#alert_danger_invalid_customer_code').removeClass('hide').show().delay(2000).fadeOut();

		  				}
		   			},'json');
		  		}

		   });

		  $("#addTimeExtend").on('click',function(){
		  		var code = $('#customercode').val();
		  		var hours = $('#hoursextend').val();
		  		var amount = $('#amountextend').val();


		  		

		  	$.confirm({
              title: 'Confirmation ',
              // content: 'Simple confirm!',
              buttons: {
              confirm: function () {
                 if(!isEmpty(code) || !isEmpty(hours) || !isEmpty(amount) ){
		  			$('#alert_danger_empty_customer_code').removeClass('hide').show().delay(2000).fadeOut();
		  		}else if(hours > <?=utils::max_hours?>){
		  			$('#alert_danger_invalid_time_customer_code').removeClass('hide').show().delay(2000).fadeOut();
		  		}else{
		  			$.post(base_url + "registration/extendTime/",{'code':code,'hours':hours,'amount':amount}, function(data) {
		  				$('#alert_success_extend_time').removeClass('hide').show().delay(2000).fadeOut();
		  				$('#hoursextend').val('');
		  				$('#amountextend').val('');
		  				$('#customercode').val('');


		  				$('#customercode').attr("disabled", false);
	  					$('#customerCodeVerification').show();
	  					$('#addTimeExtend').hide();
	  					$('.extendtime').hide();
		   			},'json');
		  		}
              },
              cancel: function () {
                  // $.alert('Canceled!');
              }, 
              }
              });


		   });


		   $("#saveprofie").on('click',function(){
		   		var password = $('#oldpassword').val();
		   		var newpasword = $('#newpassword').val();
		   		var verifynewpassword = $('#verifynewpassword').val();
		   		var input = {'password':password,'newpasword':newpasword,'verifynewpassword':verifynewpassword};

		   		var empty = 0;

		        $.each( input, function( key, value ) {
		            if(isEmpty(value)){
		              empty++;
		            }
		        });



		switch(true)
        {
          case Object.keys(input).length != empty:
          		$('#alert_danger_empty_profile').removeClass('hide').show().delay(2000).fadeOut();
           	   break;
          case password != oldpassword:
          		$('#alert_danger_invalid_profile').removeClass('hide').show().delay(2000).fadeOut();
           	   break;
          case newpasword != verifynewpassword:
          		$('#alert_danger_mismatched_profile').removeClass('hide').show().delay(2000).fadeOut();
           	   break;
          default:
         
            $.post(base_url + "setting/saveProfile/",{'newpasword':newpasword}, function(data) {
                if(data){
                    $('#alert_success_profile').removeClass('hide').show().delay(2000).fadeOut();
                    document.getElementById("changepassword").reset();
                    $(".buttonNext").trigger("click");
                }
            },'json');
            break;    
        }
	});
});


    </script>
