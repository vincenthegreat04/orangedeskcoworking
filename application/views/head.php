  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>OrangeDesk </title>
	
	<!-- Logo Icon -->
	<link rel="icon" type="image/gif/png" href="<?php echo base_url().'assets/production/images/orangedesk.jpg'?>">

	<!-- Bootstrap -->
	<link  href="<?php echo base_url().'assets/vendors/bootstrap/dist/css/bootstrap.min.css'?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url().'assets/vendors/font-awesome/css/font-awesome.min.css'?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url().'assets/vendors/nprogress/nprogress.css'?>" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="<?php echo base_url().'assets/vendors/bootstrap-daterangepicker/daterangepicker.css'?>" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?php echo base_url().'assets/build/css/custom.css'?>" rel="stylesheet">

	<!-- iCheck -->
	<link href="<?php echo base_url().'assets/vendors/iCheck/skins/flat/green.css'?>" rel="stylesheet">
	<!-- Datatables -->
	<link href="<?php echo base_url().'assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'?>" rel="stylesheet">

	 <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url().'assets/vendors/google-code-prettify/bin/prettify.min.css'?>" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo base_url().'assets/vendors/select2/dist/css/select2.min.css'?>" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo base_url().'assets/vendors/switchery/dist/switchery.min.css'?>" rel="stylesheet">
    <!-- starrr -->
    <link href="<?php echo base_url().'assets/vendors/starrr/dist/starrr.css'?>" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url().'assets/vendors/bootstrap-daterangepicker/daterangepicker.css'?>" rel="stylesheet">

    <style type="text/css">
    	
    	.buttonNext, .buttonPrevious, .buttonNew {
           
            width: 100px;
           
        }

    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <!-- <link href="<?php echo base_url().'assets/build/css/custom.min.css'?>" rel="stylesheet"> -->
  </head>

   

    <!-- Custom Theme Style -->
    