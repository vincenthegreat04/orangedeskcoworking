<!DOCTYPE html>
<html lang="en">
	<?php
		$this->load->view('head');
	?>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				
	<?php
		$this->load->view('nav');
	?>

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2><small>Setup google account emailer</small></h2>
										
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										
									<div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                
                  
                  <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Gmail Account <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" id="email" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="password" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="password" class="form-control col-md-7 col-xs-12" type="text" name="password">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <center><button type="submit" class="btn btn-success">Save</button></center>
                        </div>
                      </div>
                    </form>
                  </div>
                
              </div>
            </div>
										
					
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
					<div class="pull-right">
						Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
					</div>
					<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>

	<?php
		$this->load->view('foot');
	?>






	</body>
</html>
