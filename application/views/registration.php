<!DOCTYPE html>
<html lang="en">
  
  <?php
      $this->load->view('head');
  ?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        

        <?php
    $this->load->view('nav');
  ?>

      

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <small><span class="fa fa-clock-o"></span> Registration Steps</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    <!-- <p>Fill up</p> -->
                    <div id="wizard" class="form_wizard wizard_horizontal" >

                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no step_one">1</span>
                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Choose record type</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              Step 2<br />
                                              <small>Fill up all fields</small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no step_three">3</span>
                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Authentication Processing</small>
                                          </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-1" >
                        <form class="form-horizontal form-label-left" id="form_one">


                  

                          <div class="form-group">
                            <label class="control-label col-md-5 col-sm-3 col-xs-12" for="first-name">Record Type <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                             <div class="radio">
                                <label>
                                  <input type="radio" class="flat" checked name="record_type" value="<?=utils::solo?>"> Solo
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" class="flat" name="record_type" value="<?=utils::group?>"> Seminar
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" class="flat" name="record_type" value="<?=utils::hall?>"> Orange Room
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" class="flat" name="record_type" value="<?=utils::weekly?>"> Weekly Pass
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" class="flat" name="record_type" value="<?=utils::monthly?>"> Monthly Pass
                                </label>
                              </div>
                              <div class="radio">
                                <label>
                                  <input type="radio" class="flat" name="record_type" value="<?=utils::free?>"> Free Pass
                                </label>
                              </div>

                            </div>
                          </div>
                        </form>
                        <div style="height: 10px"></div>

                      </div>
                      <div id="step-2" style="height: 380px" >

                          

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_empty' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Fill empty fields!</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Invalid email!</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_date' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Invalid date range!</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_invalid_days' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Invalid number of days</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_mismatch' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Mismatch email!</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_maxhours' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Invalid number of hours</center></strong>
                          </div>

                          <div class="alert alert-danger alert-dismissible fade in hide" id='alert_danger_passcode' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Code does not exists or already expired</center></strong>
                          </div>

                          <div class="alert alert-success alert-dismissible fade in hide" id='alert_succes_wait' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                           <strong><center>Please wait, sending verification code may take few seconds</center></strong>
                          </div>

                         

                        
                        <form class="form-horizontal form-label-left"  novalidate onsubmit="yourJsFunction();return false" id="form_registration">


                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="name" class="form-control col-md-7 col-xs-12"  name="name" placeholder="both name(s) e.g Juan Dela Cruz"  type="text">

                               <input id="currentdatetime" value="<?=$this->utils->getNowForMysql()?>" name="currentdatetime"   type="hidden">
                            </div>
                          </div>  
                          
                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-12">
                            </div>
                          </div>

                           <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profession">Profession <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                             <select name="profession" id="profession"  class="form-control col-md-7 col-xs-12">
                                <option value="<?=utils::professional?>">Professional</option>
                                <option value="<?=utils::student?>">Student</option>
                             </select>
                            </div>
                          </div>

                          <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contactnumber"> Contact Number  <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="contactnumber" class="form-control col-md-7 col-xs-12" name="contactnumber"   type="text">
                            </div>
                          </div>



                          <div class="item form-group hide no_hours_solo">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hours_solo">Number Hours <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="no_hours_solo" id="no_hours_solo"  class="form-control col-md-7 col-xs-12">
                                 <option value="<?=utils::day?>">One day</option>
                                 <option value="<?=utils::promo?>">Promo</option>
                                 <option value="<?=utils::vip?>">VIP</option>
                                 <option value="<?=utils::one?>">One Hour</option>
                                 <option value="<?=utils::two?>">Two Hours</option>
                            </select>
                            </div>
                          </div>

                          <div class="item form-group no_hours hide">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_hours" > Number Hours 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                               <select name="no_hours" id="no_hours"  class="form-control col-md-7 col-xs-12">
                                  <?php
                                    for ($i=1; $i <= 24; $i++) { 
                                      ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                      <?php
                                    }
                                  ?>
                                </select>
                            </div>
                          </div>

                          <div class="item form-group hide date_time">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Date <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="input-prepend input-group">
                               

                                  <input type="text" class="form-control " id="single_cal2" p>
                                <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                
                          

                                </div>
                            </div>
                          </div>

                          <div class="item form-group hide free_pass_code">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Code <span class="required" style='color: red'>*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text"  name="freepasscode" id="freepasscode" class="form-control col-md-7 col-xs-12" />
                            </div>
                          </div>


                          <div class="item form-group amount hide">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount" > Rates 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input id="amount" class="form-control col-md-7 col-xs-12" name="amount"   type="text" disabled="true"  >
                            </div>
                          </div>

                          <div class="form-group">
                        <div class="col-md-12 col-md-offset-3">
                          <!-- <button type="reset" class="btn btn-primary">Reset</button> -->
                          <button type="button" class="btn btn-success" id="createRegistration">Submit</button>
                        </div>
                      </div>

                       </form>

                     


                      </div>
                       <div id="step-3">

                        <!--  <div class="alert alert-success alert-dismissible fade in hide " id='alert_success' role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong><center></center></strong>
                          </div> -->

                        <h4><center>A verification code has been sent to the email address that you provided and please proceed to the receptionist for the code verification.</center></h4>
                        
                       </div>
                      

                    </div>
                    <!-- End SmartWizard Content -->




                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            OrangeDesk Coworking Space And Study Lounge
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <?php
      $this->load->view('foot');
    ?>

	 
   <script type="text/javascript">
     
    $(document).ready(function() {


      var base_url = "<?php echo base_url(); ?>";
      $('.buttonNew').hide();

      $(".buttonNew").on('click',function(){
          $('.buttonNext').show();
          $('.buttonPrevious').show();
          $('.buttonNew').hide();
          $(".step_one").trigger("click");
      });

      $(".step_no").on('click',function(){
          $('.buttonNext').show();
          $('.buttonPrevious').show();
          $('.buttonNew').hide();
      });

      

      var currentdatetime = $('#currentdatetime').val();
      
       $('#profession').on('change', function() {
          var record_type = $('input[name=record_type]:checked').val();
          
          if(record_type == '<?=utils::solo?>'){
            var no_hours = $('#no_hours_solo').val();

            $.post(base_url + "registration/getRateAmount/",{'profession':this.value,'record_type':record_type,'no_hours':no_hours}, function(data) {
              $('#amount').val(data);
            },'json');

          }else if(record_type == '<?=utils::group?>' || record_type == '<?=utils::hall?>'){
            var no_hours = $('#no_hours').val();

            $.post(base_url + "registration/getRateAmount/",{'profession':this.value,'record_type':record_type,'no_hours':no_hours}, function(data) {
              $('#amount').val(data);
            },'json');
          }else if(record_type == '<?=utils::weekly?>' || record_type == '<?=utils::monthly?>'){
            $.post(base_url + "registration/getRateAmount/",{'profession':this.value,'record_type':record_type,'no_hours':no_hours}, function(data) {
              $('#amount').val(data);
            },'json');
          }
       });

       $('#no_hours_solo').on('change', function() {
        var  record_type = $('input[name=record_type]:checked').val();
        var  profession = $('#profession').val();
          
          $.post(base_url + "registration/getRateAmount/",{'profession':profession,'record_type':record_type,'no_hours':this.value}, function(data) {
              $('#amount').val(data);
          },'json');

       });

       // $('#no_hours').on('change', function() {
       //  var  record_type = $('input[name=record_type]:checked').val();
       //  var  profession = $('#profession').val();
       //  var  no_hours = $('#no_hours').val();

       //    $.post(base_url + "registration/getRateAmount/",{'profession':profession,'record_type':record_type,'no_hours':this.value}, function(data) {
       //        $('#amount').val(no_hours*data);
       //    },'json');
          
       // });

      

        $(".buttonNext, .step_no, .buttonPrevious").on('click',function(){
         var record_type = $('input[name=record_type]:checked').val();
         var profession = $('#profession').val();
          $("#createRegistration").attr("disabled", false);
         var no_hours;
            switch(true){
              case record_type == <?=utils::solo?>:
                $('.no_hours_solo').removeClass('hide').show();
                $('.amount').removeClass('hide').show();
                $('.no_hours').hide();
                $('.date_time').hide();
                $('.free_pass_code').hide();
                no_hours = $('#no_hours_solo').val();

                $.post(base_url + "registration/getRateAmount/",{'profession':profession,'record_type':record_type,'no_hours':no_hours}, function(data) {
                  $('#amount').val(data);
                },'json');

              break;
              case record_type != <?=utils::solo?>:
                $('.no_hours').removeClass('hide').show();
                $('.amount').removeClass('hide').show();
                $('.no_hours_solo').hide();
                $('.date_time').hide();
                $('.free_pass_code').hide();
                no_hours = $('#no_hours').val();

                if(record_type == <?=utils::weekly?> || record_type == <?=utils::monthly?>){
                  // $('.date_time').removeClass('hide').show();
                  $('.amount').removeClass('hide').show();
                  $('.no_hours_solo').hide();
                  $('.no_hours').hide();
                  $('.free_pass_code').hide();
                  // no_hours = 1;
                }

                if(record_type == <?=utils::free?>){
                  $('.no_hours_solo').hide();
                  $('.no_hours').hide();
                  $('.date_time').hide();
                  $('.amount').hide();
                  $('.free_pass_code').removeClass('hide').show();
                  no_hours = 1;
                }
                $.post(base_url + "registration/getRateAmount/",{'profession':profession,'record_type':record_type}, function(data) {
              $('#amount').val(data);
          },'json');
              break;
            }
        });

        $("#createRegistration").on('click',function(){
            var record_type = $('input[name=record_type]:checked').val();
            var name = $('#name').val();
            var email = $('#email').val();
            var profession = $('#profession').val();
            var contactnumber = $('#contactnumber').val();
            var no_hours; var date = currentdatetime; 
            var amount = $('#amount').val();
            var freepasscode = 1234569;

            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var currentdate = d.getFullYear() + '-' +(month<10 ? '0' : '') + month + '-' +(day<10 ? '0' : '') + day;

            if(record_type == '<?=utils::solo?>'){
              no_hours = $('#no_hours_solo').val();
            }else if(record_type == <?=utils::group?> || record_type == <?=utils::hall?>){
              no_hours = $('#no_hours').val();
            }else if(record_type == <?=utils::free?>){
              no_hours = 0;
              freepasscode = $('#freepasscode').val();
            }else{
              no_hours = 0;
            }
           

            var notempty = 0; 
            
            var input = {'record_type':record_type,'name':name,'email':email,'profession':profession,'contactnumber':contactnumber,'no_hours':no_hours,'amount':amount,'freepasscode':freepasscode};

            $.each( input, function( key, value ) {
                if(isEmpty(value)){
                  notempty++;
                }
            });

           
           
          switch(true)
          {
          case Object.keys(input).length != notempty:
            $('#alert_danger_empty').removeClass('hide').show().delay(2000).fadeOut();
            break;
          case isValidEmailAddress(email) == false:
            $('#alert_danger_invalid').removeClass('hide').show().delay(2000).fadeOut();
            break; 
          case currentdate > date && ('<?=utils::monthly?>'|| '<?=utils::weekly?>'):
            $('#alert_danger_invalid_date').removeClass('hide').show().delay(2000).fadeOut();
            break;
          default:
            $('#alert_succes_wait').removeClass('hide').show();
             $("#createRegistration").attr("disabled", true);
            $.post(base_url + "registration/saveRegistration/",{'input':input,'date':date}, function(data) {
                if(data){

                    $('.buttonNew').show();
                    $(".buttonNext").trigger("click");
                    $('.buttonNext').hide();
                    $('.buttonPrevious').hide();

                    $('#alert_succes_wait').hide();
                    $('#alert_success').removeClass('hide').show().delay(2000);
                    document.getElementById("form_registration").reset();
                    
                }else{
                  $('#alert_danger_passcode').removeClass('hide').show().delay(2000).fadeOut();
                  $('#alert_succes_wait').hide();

                }
            },'json');
            break;
          }

        })




  })

function treatAsUTC(date) {
    var result = new Date(date);
    result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
    return result;
}

function daysBetween(startDate, endDate) {
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
}

function myFunction(a,b,c) { return ( a + b ) - c; }


   </script>
  </body>
</html>
