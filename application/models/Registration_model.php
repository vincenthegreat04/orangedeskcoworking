<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/models/Base_model.php';

class Registration_model extends Base_model {

	protected $transaction = 'transactions';
	public $transaction_history = 'transaction_history';
	public $rates_details = 'rates_details';

	public function saveRegistration($data,$hours,$amount,$record_type)
	{
		try {

			switch (true) {
				case in_array($record_type, array(utils::solo,utils::group,utils::hall)):

					if($record_type == utils::solo && $hours == utils::vip){
						$hours = $hours - 1;
					}

					$process = 'Avail '.$hours.' hrs at amount of '.$amount;
					break;
				case $record_type == utils::weekly:
					$process = 'Avail weekly pass at amount of '.$amount;
					break;
				case $record_type == utils::monthly:
					$process = 'Avail monthly pass at amount of '.$amount;
					break;
				case $record_type == utils::free:
					$process = 'This transaction is free pass';
					break;
				default:
					# code...
					break;
			}

			$transaction = $this->db->insert($this->transaction, $data);
			$transaction_id = $this->db->insert_id();
			if($transaction){
				
				$history = array(
					'transaction_id' => $transaction_id,
					'process' => $process,
					'date' => $this->utils->getNowForMysql(),
					'modify_by' =>$this->session->userdata('user_id')
				);
				$this->db->insert($this->transaction_history, $history);
			}
			return true;
			
		} catch (Exception $e) {
			return false;
		}
	}

	public function getAllOccupation(){

		$this->db->select("*");
      	$this->db->from('occupation');
      	$this->db->order_by('name', 'ASC');
      	$query = $this->db->get();

        $data = array();
        foreach($query->result() as $row) {
			$data[] = array(
				'id' => $row->occupation_id,
				'name' => $row->name,
			);
		}

		return $data;

	}


	public function getDetailByCode($code){

		$code = $this->utils->getTodayFormat().'-'.$code;
		$query = $this->db->query('SELECT transaction_id FROM transactions WHERE  status NOT IN (1,5) and verification_code = "'.$code.'" ');
		$result = false;
		if($query->num_rows()){
			$result = true;
		}
		return $result;
	}

	public function getTransactionDetails($column,$value){
		$this->db->select("*");
      	$this->db->from('transactions');
      	$this->db->where($column, $value);
      	return $query = $this->db->get()->row();
	}

	public function getRateAmount($data){

		$this->db->select("*");
      	$this->db->from('rates_details');
      	$this->db->where($data);
      	$query = $this->db->get();

      	if($query->num_rows() > 0){
      		$row = $query->result();
      		return $row[0]->rates_amount;
      	}else{
      		return 0;
      	}
	}

	public function validateCode($data){

		$this->db->select("*");
      	$this->db->from('transactions');
      	$this->db->where($data);
      	$this->db->where('date_to >=', $this->utils->getNowForMysql());
      	$this->db->where_in('record_type',array(utils::weekly,utils::monthly,utils::free));
      	$this->db->where_not_in('status',array(utils::transaction_pending,utils::transaction_done));
      	$query = $this->db->get();

      	if($query->num_rows() > 0){
      		
      		$row = $query->result();
      		$data = array(
				'transaction_id' => $row[0]->transaction_id,
				'process' => 'Re-Login',
				'date' => $this->utils->getNowForMysql(),
				'modify_by' =>$this->session->userdata('user_id')
			);

		return $result  = $this->db->insert('transaction_history', $data);

      	}else{
      		return false;
      	}
	}

	public function isFreeCodeExists($freecode){
		$query = $this->db->select("free_code_id");
      	$this->db->from('free_code');
      	$this->db->where('code', $freecode);
      	$this->db->where('status', utils::active);

        $result = false;
		if($this->db->get()->num_rows() > 0){
			$result = true;
		}
		return $result;
	}

	public function getNumberHours($code){
		$this->db->select("hours");
      	$this->db->from('free_code');
      	$this->db->where('code', $code);
      	return   $this->db->get()->row()->hours;
	}

	public function updatePassCode($code){
		$this->db->where('code',$code);
		return $this->db->update('free_code', array('status' => utils::inactive, 'updated_at' => $this->utils->getNowForMysql()) );
	}


}