<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Base_model extends CI_Model {

	public function getNameModify($user_id){
		$this->db->select("name");
      	$this->db->from('users');
      	$this->db->where('user_id', $user_id);
      	return $this->db->get()->row()->name;
	}

	public function getOccupationName($occupation_id){
		$this->db->select("name");
      	$this->db->from('occupation');
      	$this->db->where('occupation_id', $occupation_id);
      	return $this->db->get()->row()->name;
	}


	

}
