<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class setting_model extends CI_Model {

	protected $user = 'users';

	/**
	 * overview : get all users
	 *
	 * 
	 * @return array
	 */
	public function getUser() 
	{
		$this->db->select("*");
      	$this->db->from($this->user);
      	$this->db->where('role', utils::admin);
      	$query = $this->db->get();

        $data = array();
		foreach($query->result() as $row) {

			 
			if($row->status != utils::active){
				$methodName = 'activateUser';
				$icon = 'fa-check';
				$button = 'success';
				$name = 'Active';
			}else{
				$methodName = 'deactivateUser';
				$icon = 'fa-close';
				$button = 'danger';
				$name = 'Inactive';
			}

			$data[] = array(
				$row->name,
				$row->email,
				$status = ($row->status == utils::active ? 'Active' : 'Inactive'),
				$row->created_at,
				'<center>
					<a href="#"  class="btn btn-'.$button.' btn-xs '.$methodName.'" id='.$row->user_id.'><span class="fa '.$icon.'">  </span> '.$name.' </a>
					<a href="#" class="btn btn-primary btn-xs default-password" id='.$row->user_id.' ><span class="fa fa-pencil"></span>  Default</a>
				<center>
				'
	        );
	    }

	    $result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

	/**
	 * overview : add new user
	 *
	 * @param $data
	 * @return bool
	 */
	public function addUser($data) {
		try {
			return $this->db->insert($this->user, $data);
			
		} catch (Exception $e) {
			return FALSE;
		}
	}

	/**
	 * overview : activate user
	 *
	 * @param int
	 * @return boolean
	 */
	public function activateUser($user_id) {
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('status' => utils::active, 'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()));
	}

	/**
	 * overview : deactivate user
	 *
	 * @param int
	 * @return boolean
	 */
	public function deactivateUser($user_id) {
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('status' => utils::inactive, 'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()));
	}

	/**
	 * overview : get user details
	 *
	 * @param int
	 * @return array
	 */
	public function getUserDetailsById($user_id) {
		$this->db->select("*");
      	$this->db->from($this->user);
      	$this->db->where('user_id', $user_id);
        return $this->db->get()->row();
	}
	

	public function isEmailExist($email){
		$query = $this->db->select("user_id");
      	$this->db->from($this->user);
      	$this->db->where('email', $email);

        $result = false;
		if($this->db->get()->num_rows() > 0){
			$result = true;
		}
		return $result;
	}

	public function getPassword(){
		$this->db->select("password");
      	$this->db->from('users');
      	$this->db->where('user_id', $this->session->userdata('user_id'));
      	$password =  $this->db->get()->row()->password;

      return $this->utils->_base64_decrypt($password);
	}

	public function saveProfile($data){
		$this->db->where('user_id',$this->session->userdata('user_id'));
		return $this->db->update($this->user, $data );
	}

	public function defaultPassword($user_id){
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('password' => $this->utils->_base64_encrypt(utils::default_password),'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()) );
	}

	
}
