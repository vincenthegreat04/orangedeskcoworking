<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registration_model extends CI_Model {

	protected $transaction = 'transactions';
	public $transaction_history = 'transaction_history';

	public function saveRegistration($data,$member,$registered_name)
	{
		try {
			$transaction = $this->db->insert($this->transaction, $data);
			$transaction_id = $this->db->insert_id();
			if($transaction){
				$history = array(
					'transaction_id' => $transaction_id,
					'process' => 'registered',
					'date' => $this->utils->getNowForMysql(),
					'modify_by' =>$this->session->userdata('user_id')
				);
				$this->db->insert($this->transaction_history, $history);

				if($member != false){

					if(strpos($member, ',') === false){

						$group = array(
								'transaction_id' => $transaction_id,
								'member_name' => $member,
								'status' => utils::active
							);
						$this->db->insert('member', $group);

					}else{
						$member = explode(',', $member);
						$members = array_push($member,$registered_name);

						foreach ($members as $member) {

							$group = array(
								'transaction_id' => $transaction_id,
								'member_name' => $member,
								'status' => utils::active
							);
							$this->db->insert('member', $group);
						}

					}

				}
			}
			return true;
			
		} catch (Exception $e) {
			return false;
		}
	}

}