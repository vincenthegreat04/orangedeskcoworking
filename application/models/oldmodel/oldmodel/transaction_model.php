<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transaction_model extends CI_Model {

	protected $transaction = 'transactions';
	protected $transaction_history = 'transaction_history';

	public function getTransaction($startdate,$enddate)
	{
		// $date = new DateTime($startdate);
		// // $startdate = $date->format('Y-m-d H:i:s');

		// $date = new DateTime($enddate);
		// $enddate = $date->format('Y-m-d H:i:s');

		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->where('status !=', utils::transaction_pending);
      	$this->db->where('verify_date >=',$startdate);
		$this->db->where('verify_date <=',$enddate);
      	$query = $this->db->get();

      	$data = array();
		foreach($query->result() as $row) {
			

			$data[] = array(
				$row->pass_code,
				$row->name,
				$this->utils->getRecordType($row->record_type),
				$row->verify_date,
				// $this->utils->getNewDateTime($row->verify_date,$row->hours),
				// $this->utils->getTimeOverdue($row->verify_date,$row->hours),
				// $this->utils->getTimeRemaining($row->verify_date,$row->hours),
				$this->utils->getTrasactionStatus($row->status,$row->verify_date,$row->hours,$row->settled_date),
				'<center><a href="'.base_url().'transaction/detail/'.$row->transaction_id.'" class="btn btn-primary btn-xs"><span class="fa fa-list"></span> Details</a>
				',
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

	public function verifyCode($code) 
	{

		$query = $this->db->query('SELECT transaction_id FROM transactions WHERE verification_code = "'.$code.'" ');

		$result = false;

		if($query->num_rows()){

			$transaction_id = $query->row()->transaction_id;
			

			$data = array(
				'status' => utils::transaction_validate,
				'verify_date' => $this->utils->getNowForMysql(),
				'pass_code' => $this->getEntryFormat().'-'.$this->getQueueNumber(),
				'updated_at' => $this->utils->getNowForMysql(),
				'modify_by' => $this->session->userdata('user_id')
			);

			
		if(!$this->isAlreadyVerified($code)){
			$result =  $this->updateTransaction('verification','verification_code',$code,$data,$transaction_id,null);			
		}

	}
		return $result;
	}

	public function updateTransaction($method,$column,$value,$data,$transaction_id) {
		$this->db->where($column,$value);
		$this->db->update($this->transaction, $data);


		switch ($method) {
		    case 'verification':
		        $process = 'code verified';
				break;
			case 'payment':
		    	$process = 'transaction settled';
		    default:
		}

		 $data = array(
			'transaction_id' => $transaction_id,
			'process' => $process,
			'date' => $this->utils->getNowForMysql(),
			'modify_by' =>$this->session->userdata('user_id')
		);

		return $result  = $this->db->insert($this->transaction_history, $data);
	}

	public function isAlreadyVerified($code) {
		$query = $this->db->select("transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->where('verification_code', $code);
      	$this->db->where('status >=', utils::transaction_validate);

      	$result = false;
      	if($this->db->get()->num_rows() > 0){
      		$result = true;
      	}

      	return $result;
	}

	public function getQueueNumber() {
		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->like('pass_code',$this->getEntryFormat(),'both');

      	$query=$this->db->get();
      	$result = 1;

      	if($query->num_rows() > 0){
      		$result = $query->num_rows();
      	}

      	return $result;
	}

	public function getEntryFormat() 
	{
		return $this->utils->getCurrentYear().$this->utils->getCurrentMonth().$this->utils->getCurrentDay();
	}

	public function getTransactionHistory($transaction_id){

		$this->db->select("process,date,modify_by");
      	$this->db->from('transaction_history');
      	$this->db->where('transaction_id', $transaction_id);
      	$query = $this->db->get();

      	

      	$data = array();
      	foreach($query->result() as $row) {

			$data[] = array(
				'process' => $row->process,
				'date' => $row->date,
				'name' => ($row->modify_by != null ? $this->getNameModify($row->modify_by) : ''),
			);
		}

		return $data;

	}

	public function getTransactionDetails($transaction_id){

		$query = $this->db->select("*");
          		 $this->db->from($this->transaction);
          		 $this->db->where('transaction_id', $transaction_id);
        return $this->db->get()->row();
	}

	public function getNameModify($user_id){
		$this->db->select("name");
      	$this->db->from('users');
      	$this->db->where('user_id', $user_id);
      	return $this->db->get()->row()->name;
	}

	public function getMembers($transaction_id){

		$this->db->select("status,member_name,member_id");
      	$this->db->from('member');
      	$this->db->where('transaction_id', $transaction_id);
      	// $this->db->where('status !=', utils::transaction_done);
      	$query = $this->db->get();
      	//$table,$select,$value,$where,$getcolumn
      	$transactionStatus = $this->getSingleData($this->transaction,'status',$transaction_id,'transaction_id');

      	

      	$data = array();
      	foreach($query->result() as $row) {

      	$status = 'Settled';

      	if($transactionStatus != utils::transaction_done){
      		$status  = ($row->status != utils::active ? '<button class="btn btn-primary btn-xs in" id="'.$row->member_id.'"><span class="fa fa-check"> In</span></button>' : '<button class="btn btn-danger btn-xs out" id="'.$row->member_id.'"><span class="fa fa-close"> Out</span></button>');
      	}

			$data[] = array(
				'status' => $status,
				'name' => $row->member_name,
			);
		}

		return $data;
	}

	public function memberStatus($member_id,$status){

		$data = array(
			'status' => $status
		);

		$method = 'in';
		if($status == utils::inactive){
			$method = 'out';
		}
		
		$this->db->where('member_id',$member_id);
		$this->db->update('member', $data);

		$name = $this->getMemberName($member_id);
		$transaction_id = $this->getSingleData('member','transaction_id',$member_id,'member_id');

		$data = array(
			'transaction_id' => $transaction_id,
			'process' => $method. ' '. $name,
			'date' => $this->utils->getNowForMysql(),
			'modify_by' =>$this->session->userdata('user_id')
		);

		return $result  = $this->db->insert($this->transaction_history, $data);
	}

	public function getMemberName($member_id){
		$this->db->select("member_name");
      	$this->db->from('member');
      	$this->db->where('member_id', $member_id);
      	return $this->db->get()->row()->member_name;
	}

	public function getSingleData($table,$select,$value,$where){
		$this->db->select($select);
      	$this->db->from($table);
      	$this->db->where($where, $value);
      	return $this->db->get()->row()->$select;
	}

	public function getIncome($record_type,$startdate,$enddate){

      $this->db->select("SUM(amount) as amt",false);
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$startdate);
      $this->db->where('verify_date <=',$enddate);
      $this->db->where('status',utils::transaction_done);
      $this->db->where('record_type',$record_type);
      $amount  = $this->db->get()->row()->amt;

      return $this->utils->getTotalEarnToday($amount);
  
	}

	public function getTotalIncome($startdate,$enddate){

	  $this->db->select("SUM(amount) as amt",false);
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$startdate);
      $this->db->where('verify_date <=',$enddate);
      $this->db->where('status',utils::transaction_done);
      $amount  = $this->db->get()->row()->amt;

      return $this->utils->getTotalEarnToday($amount);
	}

	public function getTotalAmountPaid($transaction_id){
		$this->db->select('amount');
      	$this->db->from($this->transaction);
      	$this->db->where('transaction_id', $transaction_id);
      	return $this->db->get()->row()->amount;
	}





}
