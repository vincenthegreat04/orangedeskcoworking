<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home_model extends CI_Model {

	protected  $transaction = 'transactions';

	public function getActiveMember() {

		$query = $this->db->select("transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		return $this->db->get()->num_rows();	
      	
	}

	public function getOverdueCustomer(){

		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
      	$query = $this->db->get();

      	$data = array(); $count = 0;
		foreach($query->result() as $row) {
			$isOverdue = $this->utils->isOverdue(null,$row->verify_date,$row->hours,$row->settled_date);
			if($isOverdue){
				$count++;
			}
		}
		return $count;

	}

	public function getDoneCustomer(){

		$query = $this->db->select("transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->where('status',utils::transaction_done);
		return $this->db->get()->num_rows();

	}

	public function getEarnToday(){
		

		$this->db->select("SUM(amount) as amt",false);
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->where('status',utils::transaction_done);
		$amount  = $this->db->get()->row()->amt;

		return $this->utils->getTotalEarnToday($amount);
	}

	

    public function getNewCustomer(){

    	$this->db->select("record_type,pass_code,name,verify_date");
      	$this->db->from($this->transaction);
      	$this->db->order_by('transaction_id', 'DESC');
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->limit('10');
      	$query = $this->db->get();

      	$data = array();
      	foreach($query->result() as $row) {

      		$data[] = array(
      			'pass_code' => $row->pass_code,
      			'name' => $row->name,
      			'record_type' => $this->utils->getRecordType($row->record_type),
      			'month' => $this->utils->getFullNameMonth(),
      			'day' => $this->utils->getCurrentDay(),
      			'verify_date' => $row->verify_date
      		);
      	}
      	return $data;
    }

    public function getHighPaid(){
    	$this->db->select("record_type,pass_code,name,amount,hours,settled_date");
      	$this->db->from($this->transaction);
      	$this->db->order_by('amount', 'DESC');
        $this->db->where('status',utils::transaction_done);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->limit('10');
		$query = $this->db->get();

      	$data = array();
      	foreach($query->result() as $row) {

      		$data[] = array(
      			'pass_code' => $row->pass_code,
      			'name' => $row->name,
      			'record_type' => $this->utils->getRecordType($row->record_type),
      			'month' => $this->utils->getFullNameMonth(),
      			'day' => $this->utils->getCurrentDay(),
      			'amount' => $this->utils->getTotalEarnToday($row->amount),
            'hours' => $row->hours,
      			'settled_date' => $row->settled_date
      		);
      	}
      	return $data;
    }

    public function getOverdue(){
      $this->db->select("*");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
      $this->db->where('verify_date <=',$this->utils->getNowEndDate());
        $query = $this->db->get();

      $data = array(); $count = 0;
      foreach($query->result() as $row) {
        $isOverdue = $this->utils->isOverdue($row->status,$row->verify_date,$row->hours,$row->settled_date);
        if($isOverdue){
          $data[] = array(
             'name' => $row->name,
             'time_overdue' => $this->utils->getTimeOverdue($row->verify_date,$row->hours,$row->settled_date),
             'transaction_id' => $row->transaction_id
          );
        }
      }
      return $data;
    }

    public function getTotalCurrentlyOverdue(){

      $this->db->select("*");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
      $this->db->where('verify_date <=',$this->utils->getNowEndDate());
        $query = $this->db->get();

      $data = array(); $count = 0;
      foreach($query->result() as $row) {
        $isOverdue = $this->utils->isOverdue($row->status,$row->verify_date,$row->hours,$row->settled_date);
        if($isOverdue){
          $count++;
        }
      }
      return $count;

    }

    

}