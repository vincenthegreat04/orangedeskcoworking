<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
require APPPATH.'/models/Base_model.php';

class Setting_model extends Base_model {

	protected $user = 'users';
	protected $occupation = 'occupation';
	protected $rates = 'rates';
	protected $rates_details = 'rates_details';

	/**
	 * overview : get all users
	 *
	 * 
	 * @return array
	 */
	public function getUser() 
	{
		$this->db->select("*");
      	$this->db->from($this->user);
      	$this->db->where('role !=', utils::superadmin);
      	$this->db->order_by('updated_at','DESC' );
      	$query = $this->db->get();

        $data = array();
		foreach($query->result() as $row) {

			 
			if($row->status != utils::active){
				$methodName = 'activateUser';
				$icon = 'fa-check';
				$button = 'success';
				$name = 'Active';
			}else{
				$methodName = 'deactivateUser';
				$icon = 'fa-close';
				$button = 'danger';
				$name = 'Inactive';
			}

			$action = '';
			if($this->session->userdata('role') == utils::superadmin){
				$action  = '<center>
					<a href="#"  class="btn btn-'.$button.' btn-xs '.$methodName.'" id='.$row->user_id.'><span class="fa '.$icon.'">  </span> '.$name.' </a>
					<a href="#" class="btn btn-primary btn-xs default-password" id='.$row->user_id.' ><span class="fa fa-pencil"></span>  Password</a>
					<a href="#" class="btn btn-warning btn-xs profile" id='.$row->user_id.' ><span class="fa fa-user"></span>  Profile</a>
				<center>
				';
			}

			$data[] = array(
				$row->name,
				$row->email,
				($row->role == utils::admin ? 'Admin' : 'Receptionist'),
				($row->status == utils::active ? 'Active' : 'Inactive'),
				$this->utils->getDateTimeFormat($row->updated_at),
				$this->getNameModify($row->modified_by),
				$action
	        );
	    }

	    $result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

	public function loadOccupation(){

		$this->db->select("*");
      	$this->db->from($this->occupation);
      	$this->db->order_by('updated_at','DESC');
      	$query = $this->db->get();

      	$data = array();
		foreach($query->result() as $row) {
			$data[] = array(
				$row->name,
				$this->utils->getDateTimeFormat($row->updated_at),
				$this->getNameModify($row->modify_by),
				'<center>
					<a href="#" class="btn btn-primary  edit btn-xs" id='.$row->occupation_id.' ><span class="fa fa-pencil"></span>  Edit</a>
				<center>'
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;

	}

	public function loadRates(){

		$this->db->select("*");
      	$this->db->from($this->rates);
      	$query = $this->db->get();

      	$data = array();
		foreach($query->result() as $row) {
			$data[] = array(
				$row->record_type,
				$this->utils->getDateTimeFormat($row->updated_at),
				$this->getNameModify($row->modify_by),
				'<center>
					<a href="#" class="btn btn-primary edit btn-xs" id='.$row->rates_id.' ><span class="fa fa-pencil"></span>  Edit</a>
				<center>'
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

		public function loadGeneratedCode(){

		$this->db->select("*");
      	$this->db->from('free_code');
      	$query = $this->db->get();

      	$data = array();
		foreach($query->result() as $row) {

			switch ($row->hours) {
				case 7:
					$hours = '7 days';
					break;
				case 31:
					$hours = '31 days';
					break;
				case 24:
					$hours = '1 day';
					break;
				case 1:
					$hours = '1 hr';
					break;
				case 2:
					$hours = '2 hrs';
					break;
				case 3:
					$hours = '3 hrs';
					break;
				case 4:
					$hours = '4 hrs';
					break;
				default:
					# code...
					break;
			}

			$data[] = array(
				$row->code,
				$hours,
				$row->status == utils::active ? 'Active':'Inactive',
				$this->utils->getDateTimeFormat($row->updated_at),
				$this->getNameModify($row->modify_by)
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

	public function getRatesDetail($id,$profession){
      $this->db->select("*");
      $this->db->from($this->rates_details);
      $this->db->where('rates_id',$id);
      $this->db->where('profession',$profession);
      $query = $this->db->get();

      $data = array(); 
      foreach($query->result() as $row) {
       
        
          $data[] = array(
             'rates_id' => $row->rates_id,
             'profession' => $row->profession,
             'codename' => $row->codename,
             'rate_name' => $row->rate_name,
             'hours' => $row->hours,
             'rates_amount' => $row->rates_amount
          );
        
      }
      return $data;
    }

    public function saveRates($data,$id){

  

		foreach ($data as $key => $value) {
			  	$this->db->where('codename',$key);
				$this->db->update($this->rates_details,array('rates_amount'=>$value));
		}
		
		$this->db->where('rates_id',$id);
		return $this->db->update($this->rates, array('modify_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()));
    }

	/**
	 * overview : add new user
	 *
	 * @param $data
	 * @return bool
	 */
	public function addUser($data) {
		try {
			return $this->db->insert($this->user, $data);
			
		} catch (Exception $e) {
			return FALSE;
		}
	}

	/**
	 * overview : add new user
	 *
	 * @param $data
	 * @return bool
	 */
	public function addOccupation($data) {
		try {
			return $this->db->insert($this->occupation, $data);
			
		} catch (Exception $e) {
			return FALSE;
		}
	}

	/**
	 * overview : activate user
	 *
	 * @param int
	 * @return boolean
	 */
	public function activateUser($user_id) {
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('status' => utils::active, 'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()));
	}

	/**
	 * overview : deactivate user
	 *
	 * @param int
	 * @return boolean
	 */
	public function deactivateUser($user_id) {
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('status' => utils::inactive, 'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()));
	}

	/**
	 * overview : get user details
	 *
	 * @param int
	 * @return array
	 */
	public function getUserDetailsById($user_id) {
		$this->db->select("*");
      	$this->db->from($this->user);
      	$this->db->where('user_id', $user_id);
        return $this->db->get()->row();
	}
	

	public function isEmailExist($email){
		$query = $this->db->select("user_id");
      	$this->db->from($this->user);
      	$this->db->where('email', $email);

        $result = false;
		if($this->db->get()->num_rows() > 0){
			$result = true;
		}
		return $result;
	}

	public function getPassword(){
		$this->db->select("password");
      	$this->db->from('users');
      	$this->db->where('user_id', $this->session->userdata('user_id'));
      	$password =  $this->db->get()->row()->password;

      return $this->utils->_base64_decrypt($password);
	}

	public function saveProfile($data){
		$this->db->where('user_id',$this->session->userdata('user_id'));
		return $this->db->update($this->user, $data );
	}

	public function defaultPassword($user_id){

		$role  = $this->getRoleId($user_id);

		$password = $this->utils->_base64_encrypt(utils::default_password);
		if($role == utils::receptionist){
			$password  = $this->utils->_base64_encrypt(utils::default_password_receptionist);
		}	

		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user, array('password' => $password,'modified_by' => $this->session->userdata('user_id'), 'updated_at' => $this->utils->getNowForMysql()) );
	}

	public function getRoleId($user_id){
		$this->db->select('role');
      	$this->db->from($this->user);
      	$this->db->where('user_id', $user_id);
      	return $this->db->get()->row()->role;
	}

	public function getOccupation($id){
		$this->db->select("name");
      	$this->db->from($this->occupation);
      	$this->db->where('occupation_id', $id);
      	return $this->db->get()->row()->name;
	}

	public function saveOccupation($id,$data){
		$this->db->where('occupation_id',$id);
		return $this->db->update($this->occupation, $data );
	}


	public function createFreeCode($data,$freecode){

		// print_r(array($data));exit();

		$check = $this->isFreeCodeExists($freecode);		
		try {
			if($check){
				return false;
			}
			return $this->db->insert('free_code', $data);
			
		} catch (Exception $e) {
			return FALSE;
		}
	}

	public function isFreeCodeExists($freecode){
		$query = $this->db->select("free_code_id");
      	$this->db->from('free_code');
      	$this->db->where('code', $freecode);

        $result = false;
		if($this->db->get()->num_rows() > 0){
			$result = true;
		}
		return $result;
	}

	public function getProfileDetails($user_id){

	  $this->db->select("*");
      $this->db->from($this->user);
      $this->db->where('user_id',$user_id);
      $query = $this->db->get();

      $data = array(); 
      foreach($query->result() as $row) {
       
        
          $data[] = array(
             'name' => $row->name,
             'email' => $row->email,
             'role' => $row->role,
          );
        
      }
      return $data;

	}

	public function updateUserDetail($data,$user_id){
		$this->db->where('user_id',$user_id);
		return $this->db->update($this->user,$data);
	}


}
