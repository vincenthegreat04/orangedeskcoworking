<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	protected  $transaction = 'transactions';

	public function getActiveMember() {

		$query = $this->db->select("transaction_id");
  	$this->db->from($this->transaction);
  	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
    $this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->where('flag_status',utils::active);
		return $this->db->get()->num_rows();	
      	
	}

	public function getOverdueCustomer(){

		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
    $this->db->where('flag_status',utils::active);
      	$query = $this->db->get();

      	$data = array(); $count = 0;
		foreach($query->result() as $row) {
			$isOverdue = $this->utils->isOverdue(null,$row->verify_date,$row->hours,$row->settled_date,$row->record_type);
			if($isOverdue){
				$count++;
			}
		}
		return $count;

	}

	public function getDoneCustomer(){

		$query = $this->db->select("transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
		$this->db->where('status',utils::transaction_done);
    $this->db->where('flag_status',utils::active);
		return $this->db->get()->num_rows();

	}

	public function getEarnToday(){
		
    $this->load->model('Transaction_model','tm');
		// $this->db->select("SUM(final_amount) as amt",false);
  //     	$this->db->from($this->transaction);
  //     	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		// $this->db->where('verify_date <=',$this->utils->getNowEndDate());
		// $this->db->where('status',utils::transaction_done);
		// $amount  = $this->db->get()->row()->amt;

		// return $amount;


     $this->db->select("final_amount,amount,hours,settled_date,verify_date,record_type,occupation");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
      $this->db->where('verify_date <=',$this->utils->getNowEndDate());
      $this->db->where('status !=',utils::transaction_pending);
      $this->db->where('flag_status',utils::active);
      $query = $this->db->get();

        

        $amount = 0; $remaining = 0; $registered = 0; $overdue  = 0; $covered = 0;

         foreach($query->result() as $row) {
          $overdue = $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
          $amt = $row->final_amount;
          $record_type   = $row->record_type;
          if(in_array($record_type, array(utils::weekly,utils::monthly,utils::free))){
          if(empty($amt) ){
              $amt = $row->amount;
            }
          }

          if($record_type ==  utils::solo){

            $hours = $row->hours;
          $vip = 0;
          if($hours == utils::vip){
            $hours = $row->hours - 1;
            $vip = 1;
          }



        $ratesperhour_solo =  $this->tm->getRates(
          array(
            'rates_id' => utils::solo,
            'profession' => $row->occupation,
            'hours' => utils::one,
          )
        );

        $ratesperhour_two =  $this->tm->getRates(
          array(
            'rates_id' => utils::solo,
            'profession' => $row->occupation,
            'hours' => utils::two
          )
        );



        $ratesperhour_promo =  $this->tm->getRates(
            array(
              'rates_id' => utils::solo,
              'profession' => $row->occupation,
              'hours' => $hours,
              'is_vip' => $vip
            )
          );

        $ratesperhour_day =  $this->tm->getRates(
            array(
              'rates_id' => utils::solo,
              'profession' => $row->occupation,
              'hours' => utils::day
            )
          );

        $where = array(
          'rates_id' => $record_type,
          'profession' => $row->occupation,
          'hours' => $hours,
          'is_vip' => $vip
        );

        $rates = $this->tm->getRates($where);

            if($overdue > utils::firstgracetime && empty($amt)){
              $amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$hours,$row->settled_date,$ratesperhour_solo,$record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day);
            }else{
              if(empty($amt)){
                  $amt = $row->amount;
              }
            }
          }         

          if(in_array($record_type, array(utils::group,utils::hall)) && empty($row->final_amount) ){

            $where = array(
          'rates_id' => $record_type,
          'profession' => $row->occupation
        );

        $rates = $this->tm->getRates($where);
            
            if($overdue > utils::firstgracetime && empty($amt)){
              $amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$row->hours,$row->settled_date,null,$record_type,null,null,$rates,null);
            }else{
              if(empty($amt)){
                  $amt = $row->hours * $row->amount;
              }
            }

          }

          $amount += $amt;
          // $registered +=  $row->hours * 60;
          // $remaining += $this->utils->getRemaining($row->verify_date,$row->hours,$row->settled_date);
          // $overdue += $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
          // $covered += $this->utils->totalTimeCovered($row->verify_date,$row->hours,$row->settled_date);

          // print_r(array($amount));exit();
    }

    // $data = array(
    //   'amount' => number_format($amount,2),
    //   'registered' => $this->utils->convertToHoursMins($registered),
    //   'remaining' => $this->utils->convertToHoursMins($remaining),
    //   'overdue' => $this->utils->convertToHoursMins($overdue),
    //   'covered' => $this->utils->convertToHoursMins($covered)
    // );

    return $amount;

	}

	

    public function getNewCustomer(){

    	$this->db->select("record_type,pass_code,name,verify_date,transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->order_by('transaction_id', 'DESC');
      	$this->db->where('verify_date >=',$this->utils->getNowStartDate());
		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
    $this->db->where('flag_status',utils::active);
		$this->db->limit('10');
      	$query = $this->db->get();

      	$data = array();
      	foreach($query->result() as $row) {

      		$data[] = array(
            'id' => $row->transaction_id,
      			'pass_code' => $row->pass_code,
      			'name' => $row->name,
      			'record_type' => $this->utils->getRecordType($row->record_type),
      			'month' => $this->utils->getFullNameMonth(),
      			'day' => $this->utils->getCurrentDay(),
      			'verify_date' => $this->utils->getDateTimeFormat($row->verify_date)
      		);
      	}
      	return $data;
    }

    public function getHighPaid(){
    	$this->db->select("record_type,pass_code,name,final_amount,hours,settled_date,transaction_id,verify_date");
      $this->db->from($this->transaction);
      $this->db->order_by('final_amount', 'DESC');
      $this->db->where('status',utils::transaction_done);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
  		$this->db->where('verify_date <=',$this->utils->getNowEndDate());
      $this->db->where('flag_status',utils::active);
  		$this->db->limit('10');
  		$query = $this->db->get();

      	$data = array();
      	foreach($query->result() as $row) {

      		$data[] = array(
            'id' => $row->transaction_id,
      			'pass_code' => $row->pass_code,
      			'name' => $row->name,
      			'record_type' => $this->utils->getRecordType($row->record_type),
      			'month' => $this->utils->getFullNameMonth(),
      			'day' => $this->utils->getCurrentDay(),
      			'amount' => number_format($row->final_amount,2),
            'hours' => $this->utils->convertToHoursMins($this->utils->totalTimeCovered($row->verify_date,$row->hours,$row->settled_date)),
      			'settled_date' => $this->utils->getDateTimeFormat($row->settled_date)
      		);
      	}
      	return $data;
    }

    public function getOverdue(){
      $this->db->select("*");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
      $this->db->where('verify_date <=',$this->utils->getNowEndDate());
      $this->db->where('flag_status',utils::active);
        $query = $this->db->get();

      $data = array(); $count = 0;
      foreach($query->result() as $row) {

      	$hours = $row->hours;
      	if($hours == utils::vip){
      		$hours = $row->hours - 1;
      	}

        $isOverdue = $this->utils->isOverdue($row->status,$row->verify_date,$hours,$row->settled_date,$row->record_type);
        if($isOverdue){
          $data[] = array(
             'name' => $row->name,
             'time_overdue' => $this->utils->getTimeOverdue($row->verify_date,$hours,$row->settled_date),
             'transaction_id' => $row->transaction_id
          );
        }
      }
      return $data;
    }

    public function getTotalCurrentlyOverdue(){

      $this->db->select("*");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$this->utils->getNowStartDate());
      $this->db->where('verify_date <=',$this->utils->getNowEndDate());
      $this->db->where('flag_status',utils::active);
      $query = $this->db->get();

      $data = array(); $count = 0;
      foreach($query->result() as $row) {

      	$hours = $row->hours;
      	if($hours == utils::vip){
      		$hours = $row->hours - 1;
      	}

        $isOverdue = $this->utils->isOverdue($row->status,$row->verify_date,$hours,$row->settled_date,$row->record_type);
        if($isOverdue){
          $count++;
        }
      }
      return $count;

    }


    

}