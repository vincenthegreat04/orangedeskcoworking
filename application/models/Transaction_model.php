<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

	protected $transaction = 'transactions';
	protected $transaction_history = 'transaction_history';

	public function getTransaction($startdate,$enddate)
	{
		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->where('status !=', utils::transaction_pending);
      	$this->db->where('flag_status', utils::active);
      	$this->db->where('verify_date >=',$startdate);
		$this->db->where('verify_date <=',$enddate);
		$this->db->order_by('verify_date', 'DESC');
      	$query = $this->db->get();

      	$data = array();
        
        		foreach($query->result() as $row) {
			$hours = $row->hours;
			if($hours == utils::vip){
				$hours = $hours - 1;
			}
			
			$remaining = $this->utils->getRemaining($row->verify_date,$hours,$row->settled_date);
			$overdue = $this->utils->getOverdued($row->verify_date,$hours,$row->settled_date);
			$covered = $this->utils->totalTimeCovered($row->verify_date,$hours,$row->settled_date);
			$data[] = array(
				'<center><a href="'.base_url().'transaction/detail/'.$row->transaction_id.'"  ><b> '.$row->pass_code.'</b></a>',
				$row->name,
				$this->utils->getRecordType($row->record_type),
				// $this->utils->getDateTimeFormat($row->verify_date),
				$this->utils->convertToHoursMins($hours*60,$row->record_type),
				$this->utils->convertToHoursMins($remaining,$row->record_type),
				$this->utils->convertToHoursMins($overdue,$row->record_type),
				$this->utils->convertToHoursMins($covered,$row->record_type),
				$this->utils->getTrasactionStatus($row->status,$row->verify_date,$hours,$row->settled_date,$row->record_type,$row->date_to),
				($this->session->userdata('role') != utils::receptionist ? '<a href="#" class="btn btn-danger btn-xs remove" id='.$row->transaction_id.' ><span class="fa fa-trash"></span>  Remove</a>':'')
				
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );
	    return $result;
	}

	public function verifyCode($code) 
	{

		$query = $this->db->query('SELECT transaction_id FROM transactions WHERE verification_code = "'.$code.'" ');

		$result = false;

		if($query->num_rows()){

			$transaction_id = $query->row()->transaction_id;
			

			$data = array(
				'status' => utils::transaction_validate,
				'flag_status' => utils::active,
				'verify_date' => $this->utils->getNowForMysql(),
				'pass_code' => $this->getEntryFormat().'-'.$this->getQueueNumber(),
				'updated_at' => $this->utils->getNowForMysql(),
				'modify_by' => $this->session->userdata('user_id')
			);

			
		if(!$this->isAlreadyVerified($code)){
			$result =  $this->updateTransaction('verification','verification_code',$code,$data,$transaction_id,null);			
		}

	}
		return $result;
	}



	public function updateTransaction($method,$column,$value,$data,$transaction_id,$hours = null,$amount = null) {
		$this->db->where($column,$value);
		$this->db->update($this->transaction, $data);


		switch ($method) {
		    case 'verification':
		        $process = 'Code verified';
				break;
			case 'payment':
		    	$process = 'Transaction settled';
		    	break;
		    case 'extend':
		    	$process = 'Time extend by '.$hours.'hrs at amount of '.$amount;
		    	break;
		    default:
		}

		$data = array(
			'transaction_id' => $transaction_id,
			'process' => $process,
			'date' => $this->utils->getNowForMysql(),
			'modify_by' =>$this->session->userdata('user_id')
		);

		return $result  = $this->db->insert($this->transaction_history, $data);
	}

	public function isAlreadyVerified($code) {
		$query = $this->db->select("transaction_id");
      	$this->db->from($this->transaction);
      	$this->db->where('verification_code', $code);
      	$this->db->where('status >=', utils::transaction_validate);

      	$result = false;
      	if($this->db->get()->num_rows() > 0){
      		$result = true;
      	}

      	return $result;
	}

	public function getFinalAmount($transaction_id){
		$query = $this->db->select("final_amount");
      	$this->db->from($this->transaction);
      	$this->db->where('transaction_id', $transaction_id);
      	return $this->db->get()->row()->final_amount;
	}

	public function getQueueNumber() {
		$this->db->select("*");
      	$this->db->from($this->transaction);
      	$this->db->like('pass_code',$this->getEntryFormat(),'both');

      	$query=$this->db->get();
      	$result = 1;

      	if($query->num_rows() > 0){
      		$result = $query->num_rows()+1;
      	}

      	return $result;
	}

	public function getEntryFormat() 
	{
		return $this->utils->getCurrentYear().$this->utils->getCurrentMonth().$this->utils->getCurrentDay();
	}

	public function getTransactionHistory($transaction_id){

		$this->db->select("process,date,modify_by");
      	$this->db->from('transaction_history');
      	$this->db->where('transaction_id', $transaction_id);
      	$this->db->order_by('date', 'DESC');
      	$query = $this->db->get();

      	

      	$data = array();
      	foreach($query->result() as $row) {

			$data[] = array(
				$this->utils->getDateTimeFormat($row->date),
				$row->process,
				($row->modify_by != null ? $this->getNameModify($row->modify_by) : 'Customer'),
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );

	    return $result;

	}

	public function getTransactionDetails($transaction_id){

		$query = $this->db->select("*");
          		 $this->db->from($this->transaction);
          		 $this->db->where('transaction_id', $transaction_id);
        return $this->db->get()->row();
	}

	public function getNameModify($user_id){
		$this->db->select("name");
      	$this->db->from('users');
      	$this->db->where('user_id', $user_id);
      	return $this->db->get()->row()->name;
	}

	public function getMembers($transaction_id){

		$this->db->select("status,member_name,member_id");
      	$this->db->from('member');
      	$this->db->where('transaction_id', $transaction_id);
      	$query = $this->db->get();
      	$transactionStatus = $this->getSingleData($this->transaction,'status',$transaction_id,'transaction_id');

      	

      	$data = array();
      	foreach($query->result() as $row) {

      	$status = 'Settled';

      	if($transactionStatus != utils::transaction_done){
      		$status  = ($row->status != utils::active ? 'Out' : '<a href="#" class="btn btn-danger btn-xs out" id="'.$row->member_id.'"><span class="fa fa-close"></span> Out</a>');
      	}

			$data[] = array(
				$row->member_name,
				$status,
			);
		}

		$result = array(
           "recordsTotal" => $query->num_rows(),
           "recordsFiltered" => $query->num_rows(),
           "data" => $data
        );

	    return $result;

	}

	public function getCurrentAmount($transaction_id){
		$this->db->select("amount");
      	$this->db->from($this->transaction);
      	$this->db->where('transaction_id', $transaction_id);
      	return $this->db->get()->row()->amount;
	}

	public function adjustPayment($amount,$transaction_id){



		// $current_amount = $this->getCurrentAmount($transaction_id);
		// $newamount = $current_amount + $amount;

		$data = array(
			'final_amount' => $amount
		);

		$this->db->where('transaction_id',$transaction_id);
		$this->db->update($this->transaction, $data);


		$data = array(
			'transaction_id' => $transaction_id,
			'process' => 'Adjust payment by '.$amount,
			'date' => $this->utils->getNowForMysql(),
			'modify_by' =>$this->session->userdata('user_id')
		);

		return $result  = $this->db->insert($this->transaction_history, $data);

	}

	public function memberStatus($member_id,$status){

		$data = array(
			'status' => $status
		);

		$method = 'In';
		if($status == utils::inactive){
			$method = 'Out';
		}
		
		$this->db->where('member_id',$member_id);
		$this->db->update('member', $data);

		$name = $this->getMemberName($member_id);
		$transaction_id = $this->getSingleData('member','transaction_id',$member_id,'member_id');

		$data = array(
			'transaction_id' => $transaction_id,
			'process' => $method. ' '. $name,
			'date' => $this->utils->getNowForMysql(),
			'modify_by' =>$this->session->userdata('user_id')
		);

		return $result  = $this->db->insert($this->transaction_history, $data);
	}

	public function getMemberName($member_id){
		$this->db->select("member_name");
      	$this->db->from('member');
      	$this->db->where('member_id', $member_id);
      	return $this->db->get()->row()->member_name;
	}

	public function getSingleData($table,$select,$value,$where){
		$this->db->select($select);
      	$this->db->from($table);
      	$this->db->where($where, $value);
      	return $this->db->get()->row()->$select;
	}

	public function getIncome($record_type,$startdate,$enddate){

      $this->db->select("final_amount,amount,hours,settled_date,verify_date,occupation");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$startdate);
      $this->db->where('verify_date <=',$enddate);
      $this->db->where('status !=',utils::transaction_pending);
      $this->db->where('flag_status',utils::active);
      $this->db->where('record_type',$record_type);
      $query = $this->db->get();

      	

      	$amount = 0; $remaining = 0; $registered = 0; $overdue  = 0; $covered = 0;
      	foreach($query->result() as $row) {
      		
      		$overdue = $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
      		$amt = $row->final_amount;

      		if(in_array($record_type, array(utils::weekly,utils::monthly,utils::free))){
  				if(empty($amt) ){
	      			$amt = $row->amount;
	      		}
      		}

      		if($record_type ==  utils::solo){

      			$hours = $row->hours;
					$vip = 0;
					if($hours == utils::vip){
						$hours = $row->hours - 1;
						$vip = 1;
					}



      			$ratesperhour_solo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::one,
					)
				);

				$ratesperhour_two =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::two
					)
				);



				$ratesperhour_promo =  $this->tm->getRates(
						array(
							'rates_id' => utils::solo,
							'profession' => $row->occupation,
							'hours' => $hours,
							'is_vip' => $vip
						)
					);

				$ratesperhour_day =  $this->tm->getRates(
						array(
							'rates_id' => utils::solo,
							'profession' => $row->occupation,
							'hours' => utils::day
						)
					);

				$where = array(
					'rates_id' => $record_type,
					'profession' => $row->occupation,
					'hours' => $hours,
					'is_vip' => $vip
				);

				$rates = $this->tm->getRates($where);

      			if($overdue > utils::firstgracetime && empty($amt)){
      				$amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$hours,$row->settled_date,$ratesperhour_solo,$record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day);
      			}else{
      				if(empty($amt)){
      				    $amt = $row->amount;
      				}
      			}
      		}      		

      		if(in_array($record_type, array(utils::group,utils::hall)) && empty($row->final_amount) ){

      			$where = array(
					'rates_id' => $record_type,
					'profession' => $row->occupation
				);

				$rates = $this->tm->getRates($where);
      			
      			if($overdue > utils::firstgracetime && empty($amt)){
      				$amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$row->hours,$row->settled_date,null,$record_type,null,null,$rates,null);
      			}else{
      				if(empty($amt)){
      				    $amt = $row->hours * $row->amount;
      				}
      			}

      		}

      		$amount += $amt;
      		$registered +=  $row->hours * 60;
      		$remaining += $this->utils->getRemaining($row->verify_date,$row->hours,$row->settled_date);
      		$overdue += $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
      		$covered += $this->utils->totalTimeCovered($row->verify_date,$row->hours,$row->settled_date);

      		// print_r(array($amount));exit();
		}
		$data = array(
			'amount' => number_format($amount,2),
			'registered' => $this->utils->convertToHoursMins($registered),
			'remaining' => $this->utils->convertToHoursMins($remaining),
			'overdue' => $this->utils->convertToHoursMins($overdue),
			'covered' => $this->utils->convertToHoursMins($covered)
		);

      return $data;
  
	}



	public function getTotalIncome($startdate,$enddate){

   // $this->db->select("SUM(amount) as amt",false);
   //    $this->db->from($this->transaction);
   //    $this->db->where('verify_date >=',$startdate);
   //    $this->db->where('verify_date <=',$enddate);
   //    $this->db->where('status',utils::transaction_done);
   //    $amount  = $this->db->get()->row()->amt;

   //    return $this->utils->getTotalEarnToday($amount);

	  $this->db->select("final_amount,amount,hours,settled_date,verify_date,record_type,occupation");
      $this->db->from($this->transaction);
      $this->db->where('verify_date >=',$startdate);
      $this->db->where('verify_date <=',$enddate);
      $this->db->where('status !=',utils::transaction_pending);
      $this->db->where('flag_status',utils::active);
      $query = $this->db->get();

      	

      	$amount = 0; $remaining = 0; $registered = 0; $overdue  = 0; $covered = 0;

      	 foreach($query->result() as $row) {
      		
      		$overdue = $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
      		$amt = $row->final_amount;
      		$record_type   = $row->record_type;
      		if(in_array($record_type, array(utils::weekly,utils::monthly,utils::free))){
  				if(empty($amt) ){
	      			$amt = $row->amount;
	      		}
      		}

      		if($record_type ==  utils::solo){

      			$hours = $row->hours;
					$vip = 0;
					if($hours == utils::vip){
						$hours = $row->hours - 1;
						$vip = 1;
					}



      			$ratesperhour_solo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::one,
					)
				);

				$ratesperhour_two =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::two
					)
				);



				$ratesperhour_promo =  $this->tm->getRates(
						array(
							'rates_id' => utils::solo,
							'profession' => $row->occupation,
							'hours' => $hours,
							'is_vip' => $vip
						)
					);

				$ratesperhour_day =  $this->tm->getRates(
						array(
							'rates_id' => utils::solo,
							'profession' => $row->occupation,
							'hours' => utils::day
						)
					);

				$where = array(
					'rates_id' => $record_type,
					'profession' => $row->occupation,
					'hours' => $hours,
					'is_vip' => $vip
				);

				$rates = $this->tm->getRates($where);

      			if($overdue > utils::firstgracetime && empty($amt)){
      				$amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$hours,$row->settled_date,$ratesperhour_solo,$record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day);
      			}else{
      				if(empty($amt)){
      				    $amt = $row->amount;
      				}
      			}
      		}      		

      		if(in_array($record_type, array(utils::group,utils::hall)) && empty($row->final_amount) ){

      			$where = array(
					'rates_id' => $record_type,
					'profession' => $row->occupation
				);

				$rates = $this->tm->getRates($where);
      			
      			if($overdue > utils::firstgracetime && empty($amt)){
      				$amt = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$row->hours,$row->settled_date,null,$record_type,null,null,$rates,null);
      			}else{
      				if(empty($amt)){
      				    $amt = $row->hours * $row->amount;
      				}
      			}

      		}

      		$amount += $amt;
      		$registered +=  $row->hours * 60;
      		$remaining += $this->utils->getRemaining($row->verify_date,$row->hours,$row->settled_date);
      		$overdue += $this->utils->getOverdued($row->verify_date,$row->hours,$row->settled_date);
      		$covered += $this->utils->totalTimeCovered($row->verify_date,$row->hours,$row->settled_date);

      		// print_r(array($amount));exit();
		}

		$data = array(
			'amount' => number_format($amount,2),
			'registered' => $this->utils->convertToHoursMins($registered),
			'remaining' => $this->utils->convertToHoursMins($remaining),
			'overdue' => $this->utils->convertToHoursMins($overdue),
			'covered' => $this->utils->convertToHoursMins($covered)
		);

		return $data;
	}

	public function getTotalAmountPaid($transaction_id){
		$this->db->select('amount');
      	$this->db->from($this->transaction);
      	$this->db->where('transaction_id', $transaction_id);
      	return $this->db->get()->row()->amount;
	}

	public function getHours($transaction_id){
		$this->db->select('hours');
      	$this->db->from($this->transaction);
      	$this->db->where('transaction_id', $transaction_id);
      	return $this->db->get()->row()->hours;
	}

	public function getRates($where){
		$this->db->select('rates_amount');
      	$this->db->from('rates_details');
      	$this->db->where($where);
      	return $this->db->get()->row()->rates_amount;
	}


	public function removeTransaction($id){
		$this->db->where('transaction_id',$id);
		return $this->db->update($this->transaction, array('flag_status'=>utils::inactive));
	}



}
