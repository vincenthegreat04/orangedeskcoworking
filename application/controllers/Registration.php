<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

    public function __construct(){
        parent:: __construct();
        $this->load->model('Registration_model','rm');
        $this->load->model('Transaction_model','tm');
    }

    public function index()
    {
        $this->load->view('registration');
    }

    public function code()
    {
        $this->load->view('code');
    }

    public function saveRegistration()
    {
        $input =  $this->input->post('input');
        $date_from =  $this->input->post('date');
        $code = $this->utils->generateRandomPassword();

        
        $this->load->library('email');
        $this->email->set_newline("\r\n");

        $this->email->from('orangedesk.emailer@gmail.com','Emailer');
        $this->email->to($input['email']);
        $this->email->subject('Verification Code');
        $this->email->message('Your Verification Code is '.$this->utils->getTodayFormat().'-'.$code);
        $this->email->send();

        if($input['record_type'] == utils::weekly){
            $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' + 7 days'));
        }elseif($input['record_type'] == utils::monthly){
            $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' + 31 days'));
        }elseif($input['record_type'] == utils::free){
            $check = $this->rm->isFreeCodeExists($input['freepasscode']);
            
            if(!$check){
                return $this->output->set_output(json_encode(false));
            }else{
                $hours = $this->rm->getNumberHours($input['freepasscode']);
                
                switch ($hours) {
                    case 24:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' + 1 days'));
                        break;
                    case 4:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' +4 hour'));
                        break;
                    case 1:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' +1 hour'));
                        break;
                    case 2:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' +2 hour'));
                        break;
                    case 3:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' +3 hour'));
                        break;
                    case 7:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' + 7 days'));
                        break;
                    case 31:
                        $date_to = date('Y-m-d H:i:s', strtotime($date_from. ' + 31 days'));
                        break;
                    default:
                        # code...
                        break;
                }
                
            }
             $this->rm->updatePassCode($input['freepasscode']);
        }else{
            $date_to = '';
        }

        $hours = $input['no_hours'];
        if($input['no_hours'] == utils::vip && $input['record_type'] == utils::solo){
            $hours = $input['no_hours'];
        }

         $data = array (
            'record_type' => $input['record_type'],
            'name' => $input['name'],
            'email' => $input['email'],
            'hours' => $hours,
            'occupation' => $input['profession'],
            'status' => utils::transaction_pending,
            'verification_code' => $this->utils->getTodayFormat().'-'.$code,
            'created_at' => $this->utils->getNowForMysql(),
            'address' => '',
            'contact_number' => $input['contactnumber'],
            'amount' => $input['amount'],
            'date_from' => $date_from,
            'date_to' => $date_to,
            // 'modify_by' => $this->session->userdata('user_id')
         );

         $result = $this->rm->saveRegistration($data,$input['no_hours'],$input['amount'],$input['record_type']);

         $this->output->set_output(json_encode($result));

    }

    public function getAllOccupation(){
        $result = $this->rm->getAllOccupation();
        $this->output->set_output(json_encode($result));
    }

    public function getDetailByCode(){
        $code = $this->input->post('code');
        $result = $this->rm->getDetailByCode($code);
        $this->output->set_output(json_encode($result));
    }


    public function extendTime(){
        $hours = $this->input->post('hours');
        $amount = $this->input->post('amount');
        $code = $this->input->post('code');

        $result = $this->rm->getTransactionDetails('verification_code',$code);

        // print_r($result->record_type);exit();

        $data = array(
            'hours' => $hours + $result->hours,
            'amount' => $amount + $result->amount,
            'updated_at' => $this->utils->getNowForMysql(),
            'status' => utils::transaction_extend,
            // 'modify_by' => $this->session->userdata('user_id')
        );

        
        $result = $this->tm->updateTransaction('extend','transaction_id',$result->transaction_id,$data,$result->transaction_id,$hours,$amount);
        $this->output->set_output(json_encode($result));
    }

    public function getRateAmount(){

        $record_type = $this->input->post('record_type');
        if($record_type == utils::solo){

            $hours = $this->input->post('no_hours');
            $vip = 0;
            if($hours == utils::vip){
                $hours = $this->input->post('no_hours') - 1;
                $vip = 1;
            }
            $data = array(
                'profession' => $this->input->post('profession'),
                'rates_id' => $this->input->post('record_type'),
                'hours' => $hours,
                'is_vip' => $vip
            );
        }else{
            $data = array(
                'profession' =>$this->input->post('profession'),
                'rates_id' =>$this->input->post('record_type')
            );
        }

        $result = $this->rm->getRateAmount($data);
        $this->output->set_output(json_encode($result));
    }

    public function validateCode(){

        $email = $this->input->post('email');
        $code = $this->input->post('code');

        $data = array(
            'verification_code' => $code,
            'email' => $email
        );

        $result = $this->rm->validateCode($data);
        $this->output->set_output(json_encode($result));
    }


    
}

