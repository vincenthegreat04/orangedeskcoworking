<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent:: __construct();

		if(! $this->session->userdata('logged_in')){
        	return redirect('login/');
    	}

		$this->load->model('Home_model','hm');
	}

	public function index()
	{
		// $role = $this->session->userdata('role');
		$this->load->view('home');
		
	}

	public function getActiveMember(){
		$result = $this->hm->getActiveMember();
		$this->output->set_output(json_encode($result));
	}

	public function getOverdueCustomer(){
		$result = $this->hm->getOverdueCustomer();
		$this->output->set_output(json_encode($result));
	}

	public function getDoneCustomer(){
		$result = $this->hm->getDoneCustomer();
		$this->output->set_output(json_encode($result));
	}

	public function getEarnToday(){
		$result = number_format($this->hm->getEarnToday(),2);
		$this->output->set_output(json_encode($result));
	}

	public function getNewCustomer(){
		$result = $this->hm->getNewCustomer();
		$this->output->set_output(json_encode($result));
	}
	
	public function getHighPaid(){
		$result = $this->hm->getHighPaid();
		$this->output->set_output(json_encode($result));
	}

	public function getOverdue(){
		$result = $this->hm->getOverdue();
		$this->output->set_output(json_encode($result));
	}

	public function getTotalCurrentlyOverdue(){
		$result = $this->hm->getTotalCurrentlyOverdue();
		$this->output->set_output(json_encode($result));
	}
}
