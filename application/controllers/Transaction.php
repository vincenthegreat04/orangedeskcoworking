<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();

		if(! $this->session->userdata('logged_in')){
        	return redirect('login/');
    	}

		$this->load->model('Transaction_model','tm');
		$this->load->model('Base_model','bm');
	}

	public function index()
	{
		$this->load->view('transaction');
	}

	public function test()
	{
		echo	$data=$this->tm->getQueueNumber();

		
	}

	public function loadTransaction($startdate,$starttime,$enddate,$endtime)
	{
		$start = $startdate." ".$starttime;
		$end = $enddate." ".$endtime;
		// print_r(array($start,$end));exit();
		$data=$this->tm->getTransaction($start,$end);
		$this->output->set_output(json_encode($data));
	}

	public function verifyCode()
	{
		$code =  $this->input->post('code');
		$result=$this->tm->verifyCode($code);

		$this->output->set_output(json_encode($result));
	}

	public function detail($transaction_id)
	{
		$this->load->view('transaction_detail');
	}

	public function getTransactionHistory($transaction_id)
	{
		$result = $this->tm->getTransactionHistory($transaction_id);
		$this->output->set_output(json_encode($result));
	}

	public function getTransactionDetails($transaction_id)
	{
		$row = $this->tm->getTransactionDetails($transaction_id);

		if(in_array($row->record_type, array(utils::solo,utils::group,utils::hall))){
			$rates = 1; $paidamount = 0;
			switch (true) {
				
				case $row->record_type == utils::group or $row->record_type == utils::hall:
					$hours = $row->hours;
					$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation
					);

					// $paidamount = $row->final_amount;
				 // 	if(empty($row->final_amount)){
				 // 		$paidamount = $row->amount  * $row->hours;
				 // 	}

					$paidamount = $row->amount  * $row->hours;
					$vip = 0;
					break;
				case $row->record_type == utils::solo:
					$hours = $row->hours;
					$vip = 0;
					if($hours == utils::vip){
						$hours = $row->hours - 1;
						$vip = 1;
					}

					$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation,
						'hours' => $hours,
						'is_vip' => $vip
					);
					// $paidamount = $row->amount;

					// $paidamount = $row->final_amount;
				 // 	if(empty($row->final_amount)){
				 		
				 // 	}

					$paidamount = $row->amount;
					break;
				default:
				case $row->record_type == utils::weekly or $row->record_type == utils::monthly:
					// $paidamount = $row->final_amount;
				 // 	if(empty($row->final_amount)){
				 		
				 // 	}
				 	$paidamount = $row->amount;
					break;
			}

			$rates = $this->tm->getRates($where);

			// print_r(array($rates));exit();
			$ratesperhour_solo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::one
					)
				);
			$ratesperhour_two =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::two
					)
				);
			$ratesperhour_promo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::promo,
						'is_vip' => $vip
					)
				);
			$ratesperhour_day =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::day
					)
				);

			$total = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$hours,$row->settled_date,$ratesperhour_solo,$row->record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day);

			// print_r(array($total));exit();

			$data = array(
			'record_type' => $this->utils->getRecordType($row->record_type),
			'name' => $row->name,
			'email' => $row->email,
			'hours' => $this->utils->convertToHoursMins($hours*60),
			'occupation' => ($row->occupation == utils::professional ? 'Professional':'Student'),
			'status' => $row->status,
			'verify_date' => $this->utils->getDateTimeFormat($row->verify_date),
			'pass_code' => $row->pass_code,
			'modify_by' => $row->modify_by,
			'address' => $row->address,
			'contact_number' => $row->contact_number,
			'time_out' => $this->utils->getDateTimeFormat($this->utils->getNewDateTime($row->verify_date,$hours)),
			'time_remaining' => $this->utils->getTimeRemaining($row->verify_date,$row->hours,$row->settled_date),
			'time_overdue' => $this->utils->getTimeOverdue($row->verify_date,$hours,$row->settled_date),
			'transaction_status' => $this->utils->getTrasactionStatus($row->status,$row->verify_date,$hours,$row->settled_date,$row->record_type),
			'record_type_int' => $row->record_type,
			'amount' => ($row->final_amount == null ? number_format($total,2):number_format($row->final_amount,2)),
			'overdue_amount' => ( $row->final_amount == null ? number_format($total - $paidamount,2) :number_format($row->final_amount - $paidamount,2)  ), 
			'time_covered' => $this->utils->convertToHoursMins($this->utils->totalTimeCovered($row->verify_date,$hours,$row->settled_date))
			);
			// print_r(array($this->utils->getTotalEarnToday($row->amount,$row->verify_date,$hours,$row->settled_date,$ratesperhour_solo,$row->record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day),$paidamount));
			//
		}else{

			$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation
					);
			
			
			if($row->record_type ==  utils::free){
				$rates = 0;
			}else{
				$rates = $this->tm->getRates($where);
			}
			

			$data = array(
			'record_type' => $this->utils->getRecordType($row->record_type),
			'name' => $row->name,
			'email' => $row->email,
			'hours' => '-',
			'occupation' => ($row->occupation == utils::professional ? 'Professional':'Student'),
			'status' => $row->status,
			'verify_date' => $this->utils->getDateTimeFormat($row->verify_date),
			'pass_code' => $row->pass_code,
			'modify_by' => $row->modify_by,
			'address' => $row->address,
			'contact_number' => $row->contact_number,
			'time_out' =>$this->utils->getDateTimeFormat($row->date_to),
			'time_remaining' => '-',
			'time_overdue' => '-',
			'transaction_status' => $this->utils->getTrasactionStatus($row->status,$row->verify_date,$row->hours,$row->settled_date,$row->record_type,$row->date_to),
			'record_type_int' => $row->record_type,
			'amount' =>  ($row->final_amount == null ? number_format($this->utils->getTotalEarnToday($row->amount,$row->verify_date,$row->hours,$row->settled_date,null,$row->record_type,null,null,$rates,null),2):number_format($row->final_amount,2)),
			'overdue_amount' => '-',
			'time_covered' => '-'
			);
		}

		
		$this->output->set_output(json_encode($data));
	}

	public function settleTransaction($transaction_id) {

		// $amount = $this->input->post('amount');
		//

		$finalamount = $this->tm->getFinalAmount($transaction_id);

		$row = $this->tm->getTransactionDetails($transaction_id);

		if( ($row->record_type== utils::weekly and empty($row->final_amount)) or ($row->record_type== utils::monthly and empty($row->final_amount))){

			$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation
					);

			$amount = $row->amount;
			
		}elseif(($row->record_type== utils::weekly and !empty($row->final_amount)) or ($row->record_type== utils::monthly and !empty($row->final_amount))){
			$amount = $finalamount;

			// print_r(array($amount));exit();
		}elseif(!empty($finalamount)){
			$amount = $finalamount;
		}else{

			
			switch (true) {
				
				case $row->record_type == utils::group or $row->record_type == utils::hall or $row->record_type == utils::weekly or $row->record_type == utils::monthly:
					$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation
					);
					$rates = $this->tm->getRates($where);
					$vip = 0;
				break;
				case $row->record_type == utils::solo:
					$hours = $row->hours;
					$vip = 0;
					if($hours == utils::vip){
						$hours = $row->hours - 1;
						$vip = 1;
					}

					$where = array(
						'rates_id' => $row->record_type,
						'profession' => $row->occupation,
						'hours' => $hours,
						'is_vip' => $vip
					);

					$rates = $this->tm->getRates($where);
				break;
					
				default:
					$rates = 0;
					break;
			}

			
			$ratesperhour_solo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::one
					)
				);
			$ratesperhour_two =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::two
					)
				);
			$ratesperhour_promo =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::promo,
						'is_vip' => $vip
					)
				);
			$ratesperhour_day =  $this->tm->getRates(
					array(
						'rates_id' => utils::solo,
						'profession' => $row->occupation,
						'hours' => utils::day
					)
				);

			$amount = $this->utils->getTotalEarnToday($row->amount,$row->verify_date,$row->hours,$row->settled_date,$ratesperhour_solo,$row->record_type,$ratesperhour_two,$ratesperhour_promo,$rates,$ratesperhour_day);

			 // print_r(array($amount));exit();
		}

		$data = array(
			'settled_date' => $this->utils->getNowForMysql(),
			'status' => utils::transaction_done,
			'final_amount' => $amount,
			'updated_at' => $this->utils->getNowForMysql(),
			'modify_by' => $this->session->userdata('user_id')
		);

		// print_r(array($amount));exit();

		$result = $this->tm->updateTransaction('payment','transaction_id',$transaction_id,$data,$transaction_id,null);
		$this->output->set_output(json_encode($result));
	}

	public function getMembers($transaction_id) {

		$result = $this->tm->getMembers($transaction_id);
		$this->output->set_output(json_encode($result));

	}

	public function memberStatus($member_id){

		$status =  $this->input->post('status');

		
		$result = $this->tm->memberStatus($member_id,$status['status']);
		$this->output->set_output(json_encode($result));
	}

	public function adjustPayment($transaction_id){

		$amount =  $this->input->post('amount');
		// print_r($amount);exit();
		
		$result = $this->tm->adjustPayment($amount,$transaction_id);
		$this->output->set_output(json_encode($result));
	}

	public function getIncome(){
		$record_type = $this->input->post('record_type'); 
		$startdate = $this->input->post('startdate'); 
		$enddate = $this->input->post('enddate');


		$result = $this->tm->getIncome($record_type,$startdate,$enddate);
		$this->output->set_output(json_encode($result));
	}

	public function getTotalIncome(){
		$startdate = $this->input->post('startdate'); 
		$enddate = $this->input->post('enddate');
		$result = $this->tm->getTotalIncome($startdate,$enddate);
		$this->output->set_output(json_encode($result));
	}

	public function getTotalAmountPaid($transaction_id){
		$amount = $this->tm->getTotalAmountPaid($transaction_id);

		$result = $this->utils->getTotalEarnToday($amount);
		$this->output->set_output(json_encode($result));
	}

	public function extendTime($transaction_id){
		$hours = $this->input->post('hours');

		$oldhours = $this->tm->getHours($transaction_id);

		$data = array(
			'hours' => $hours + $oldhours,
			'updated_at' => $this->utils->getNowForMysql(),
			'modify_by' => $this->session->userdata('user_id')
		);
		//updateTransaction($method,$column,$value,$data,$transaction_id)
		$result = $this->tm->updateTransaction('extend','transaction_id',$transaction_id,$data,$transaction_id);
		$this->output->set_output(json_encode($result));
	}

	public function removeTransaction()
	{	
		$id = $this->input->post('id');
		$result = $this->tm->removeTransaction($id);
		$this->output->set_output(json_encode($result));
	}

	

}
