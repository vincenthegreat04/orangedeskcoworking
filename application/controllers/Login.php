<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent:: __construct();

		
		$this->load->model('Login_model','lm');
		$this->load->library('utils');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login()
	{
		$email = $this->input->post('email');
		$password = $this->utils->_base64_encrypt($this->input->post('password'));
		
		$result = $this->lm->login($email,$password);

		if($result){
			$data= array(
                   'user_id' => $result->user_id,
                   'role' => $result->role,
                   'name' => $result->name,
                   'logged_in' => true,
                   'exists' => utils::exists
        	);
         $this->session->set_userdata($data);
         	
		}else{
			$data = array('exists' => utils::nonexists);
		}
		$this->output->set_output(json_encode(array('exists'=>$data['exists'])));

		 // echo json_encode(array('exists'=>$data['exists']));
		
	}

	public function logout(){
		$this->session->unset_userdata('user_id');
		$this->session->sess_destroy();
		redirect('login/');
	}

}
		
