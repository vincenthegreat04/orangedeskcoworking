<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {


	public function __construct(){
		parent:: __construct();

		if(! $this->session->userdata('logged_in')){
        	return redirect('login/');
    	}

		$this->load->model('Setting_model','sm');
	}

	public function user()
	{
		if($this->session->userdata('role') != utils::receptionist){
			$this->load->view('user');
		}else{
			return redirect('home/');
		}
		
	}

	public function freecode()
	{
		$this->load->view('freecode');
	}

	public function occupation()
	{
		$this->load->view('occupation');
	}

	public function rates()
	{
		$this->load->view('rates');
	}

	public function loadUser()
	{
		$data=$this->sm->getUser();
		$this->output->set_output(json_encode($data));
	}

	public function loadOccupation()
	{
		$data=$this->sm->loadOccupation();
		$this->output->set_output(json_encode($data));
	}

	public function loadRates()
	{
		$data=$this->sm->loadRates();
		$this->output->set_output(json_encode($data));
	}

	public function getRatesDetail(){
		$id = $this->input->post('id');
		$profession = $this->input->post('profession');
		$result = $this->sm->getRatesDetail($id,$profession);
		$this->output->set_output(json_encode($result));
	}

	public function addUser()
	{
		$input =  $this->input->post('input');
		$isEmailExist  = $this->isEmailExist($input['email']);
		$password = $this->utils->_base64_encrypt(utils::default_password);

		if($input['role'] == utils::receptionist){
			$password = $this->utils->_base64_encrypt(utils::default_password_receptionist);
		}

		$data = array(
			'name' =>$input['name'],
			'email' =>$input['email'],
			'password' =>$password,
			'status' => utils::active,
			'created_at' => $this->utils->getNowForMysql(),
			'updated_at' => $this->utils->getNowForMysql(),
			'modified_by' => $this->session->userdata('user_id'),
			'role' => $input['role'],
		);

		if(!$isEmailExist){
			$result = $this->sm->addUser($data);
		}else{
			$result = false;
		}

		
		$this->output->set_output(json_encode($result));
	}

	public function saveRates()
	{
		$id  = $this->input->post('id');
		$input  = $this->input->post('input');

		switch ($id) {
			case utils::solo :
				$data = array(
					'p_oneday'=> $input['p_oneday'],
					'p_promo'=> $input['p_promo'],
					'p_onehour'=> $input['p_onehour'],
					'p_twohour'=> $input['p_twohour'],
					's_oneday'=> $input['s_oneday'],
					's_promo'=> $input['s_promo'],
					's_onehour'=> $input['s_onehour'],
					's_twohour'=> $input['s_twohour'],
					'p_vip'=> $input['p_vip'],
					's_vip'=> $input['s_vip']
				);
				break;
			case utils::hall:
				$data = array(
					's_p_hourly_rate'=> $input['s_p_hourly_rate'],
					's_s_hourly_rate'=> $input['s_s_hourly_rate']
				);
				break;
			case utils::group:
				$data = array(
					'o_p_hourly_rate'=> $input['o_p_hourly_rate'],
					'o_s_hourly_rate'=> $input['o_s_hourly_rate']
				);
				break;
			case utils::weekly:
				$data = array(
					'w_p_hourly_rate'=> $input['w_p_hourly_rate'],
					'w_s_hourly_rate'=> $input['w_s_hourly_rate']
				);
				break;
			case utils::monthly:
				$data = array(
					'm_p_hourly_rate'=> $input['m_p_hourly_rate'],
					'm_s_hourly_rate'=> $input['m_s_hourly_rate']
				);
				break;
			default:
				# code...
				break;
		}

		$result = $this->sm->saveRates($data,$id);
		$this->output->set_output(json_encode($result));
	}

	public function deactivateUser($user_id)
	{
		$result = $this->sm->deactivateUser($user_id);
		$this->output->set_output(json_encode($result));
	}

	public function activateUser($user_id)
	{
		$result = $this->sm->activateUser($user_id);
		$this->output->set_output(json_encode($result));
	}

	public function isEmailExist($email){
		return $result = $this->sm->isEmailExist($email);
	}

	public function getPassword(){
		$result = $this->sm->getPassword();
		$this->output->set_output(json_encode($result));
	}

	public function saveProfile(){
		
		$password  = $this->input->post('newpasword');
		$data = array(
			'password' => $this->utils->_base64_encrypt($password),
			'updated_at' => $this->utils->getNowForMysql(),
			'modified_by' => $this->session->userdata('user_id')
		);

		$result = $this->sm->saveProfile($data);
		$this->output->set_output(json_encode($result));
	}

	public function defaultPassword($user_id){
		$result = $this->sm->defaultPassword($user_id);
		$this->output->set_output(json_encode($result));
	}

	public function addOccupation(){
		$occupation  = $this->input->post('occupation');

		$data = array(
			'name' => $occupation,
			'updated_at' => $this->utils->getNowForMysql(),
			'modify_by' => $this->session->userdata('user_id')
		);

		$result = $this->sm->addOccupation($data);
		$this->output->set_output(json_encode($result));
	}

	public function getOccupation($id){

		$result = $this->sm->getOccupation($id);
		$this->output->set_output(json_encode($result));
	}

	public function saveOccupation(){
		$occupation  = $this->input->post('occupation');
		$id  = $this->input->post('id');

		$data = array(
			'name' => $occupation,
			'updated_at' => $this->utils->getNowForMysql(),
			'modify_by' => $this->session->userdata('user_id')
		);

		$result = $this->sm->saveOccupation($id,$data);
		$this->output->set_output(json_encode($result));

	}

	public function createFreeCode(){

		$freecode  = $this->input->post('freecode');
		$hours  = $this->input->post('hours');


		
		$data = array(
			'code' => $freecode,
			'hours' => $hours,
			'status' => utils::active,
			'modify_by' => $this->session->userdata('user_id'),
			'updated_at' => $this->utils->getNowForMysql()
		);

		$result = $this->sm->createFreeCode($data,$freecode);
		$this->output->set_output(json_encode($result));
	}

	public function loadGeneratedCode(){
		$data = $this->sm->loadGeneratedCode();
		$this->output->set_output(json_encode($data));
	}

	public function getProfileDetails($user_id){
		$data = $this->sm->getProfileDetails($user_id);
		$this->output->set_output(json_encode($data));
	}

	public function updateUserDetail(){
		$input =  $this->input->post('input');
		$user_id =  $this->input->post('user_id');
		$isEmailExist  = $this->isEmailExist($input['email']);
		

		$data = array(
			'name' =>$input['name'],
			'email' =>$input['email'],
			'updated_at' => $this->utils->getNowForMysql(),
			'modified_by' => $this->session->userdata('user_id'),
			'role' => $input['role']
		);

		// if(!$isEmailExist){
		// 	$result = $this->sm->updateUserDetail($data,$user_id);
		// }else{
		// 	$result = false;
		// }

		$result = $this->sm->updateUserDetail($data,$user_id);
		$this->output->set_output(json_encode($result));
	}
	



	
}
		
